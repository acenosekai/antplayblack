# README #

* Android Music Player Based on BASS audio library from un4seen developer.
* Play more files type than android native media player library.
* Support DSD music file
* This application is on heavy development.
To use this app first you need to go to library to add music folder than go to files browser to pick some file to play.


### Playable Files ###
MP4, M4A, AAC, FLAC, MP3, OGG, WAV, AIFF, APE, DFF, DSF, OPUS, SF2, SFZ, MPC, MPP, MP+, TTA

### Feature completion ###
* Now Playing (OK)
* Queue List (work --need to add header/footer for queue playback)
* Navigation (barely working -- will be changed with new one :D)
* Files Browser (OK separated to 3 section : Files, Artist, Album --may be changed)
* Playlist (OK)
* Search (OK)
* Playback Repeat (OK set via now playing)
* Playback Shuffle (OK set via now playing)
* Playback crossfade (Function ready)
* Effect Amplification (OK)
* Effect Freeverb (OK need to add preset menu)
* Effect Dynamic Amplification (OK need to add preset menu)
* Effect Compressor (OK need to add preset menu)
* Effect 10 parametric Equalizer (FunctionReady)
* Library/Music Crawler (OK)
* Advance Setting (not yet)

