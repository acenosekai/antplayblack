package com.acenosekai.antplay.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.acenosekai.antplay.R;
import com.acenosekai.antplay.adapter.MainPagerAdapter;
import com.acenosekai.antplay.realm.MediaLibrary;
import com.acenosekai.antplay.services.PlaybackService;
import com.acenosekai.antplay.sugar.BaseFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Acenosekai on 12/24/2015.
 * Rock On
 */
public class MainFragment extends BaseFragment {

//    ScreenSlidePagerAdapter mDemoCollectionPagerAdapter;
//    ViewPager mViewPager;

    private ViewPager viewPager;
    private MainPagerAdapter mAdapter;
//    private ActionBar actionBar;
    // Tab titles
//    private String[] tabs = { "Top Rated", "Games", "Movies" };

    private NavigationFragment nav;
    private NowplayingFragment now;
    private QueueFragment que;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        // Inflate the layout for this fragment
        View navView = inflater.inflate(R.layout.fragment_main, container, false);
        viewPager = (ViewPager) navView.findViewById(R.id.pager);
        viewPager.setPageMargin(-50);
//        viewPager.setHorizontalFadingEdgeEnabled(true);
//        viewPager.setFadingEdgeLength(30);
        List<Fragment> fl = new ArrayList<>();
        nav = new NavigationFragment();
        now = new NowplayingFragment();
        que = new QueueFragment();

        fl.add(nav);
        fl.add(now);
        fl.add(que);

        mAdapter = new MainPagerAdapter(getChildFragmentManager(), fl);


//        viewPager.getCurrentItem()

        final PlaybackService playbackService = activity.getPlaybackService();
        if (playbackService != null) {
            playbackService.setOnInitialize(new PlaybackService.OnInitialize() {
                @Override
                public void onSuccess() {
                    try {
                        if (viewPager.getCurrentItem() == 1) {
                            now.initVisual();
                        }
                    } catch (Exception e) {
                        playbackService.reinitService();
                    }
                }

                @Override
                public void onFailed() {
                    if (app.getRealm().where(MediaLibrary.class).findAll().isEmpty()) {
                        Toast.makeText(getActivity(), "go to library", Toast.LENGTH_SHORT).show();
                        //activity.gotoLibrary();
                    } else {
                        Toast.makeText(getActivity(), "go to files", Toast.LENGTH_SHORT).show();
                        //go to files
                    }
                }
            });

            playbackService.setOnPlayingStatusChange(new PlaybackService.OnPlayingStatusChange() {
                @Override
                public void OnPlayingStatusChange(boolean isPlaying) {
                    if (viewPager.getCurrentItem() == 1) {
                        if (isPlaying) {
                            now.playingVisual();
                            now.playMode();
                        } else {
                            now.pauseMode();
                        }
                    } else if (viewPager.getCurrentItem() == 2) {
                        que.refresh();
                    }
                }
            });

            playbackService.setOnInit(new PlaybackService.OnInitInterface() {
                @Override
                public void onInit() {
                    try {
                        if (viewPager.getCurrentItem() == 1) {
                            now.initVisual();
                        }
                        if (viewPager.getCurrentItem() == 2) {
                            que.refresh();
                        }
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }
            });

            playbackService.setOnRepeatChange(new PlaybackService.OnRepeatChangeInterface() {
                @Override
                public void onRepeatChange(int repeat) {
                    if (viewPager.getCurrentItem() == 1) {
                        now.repeatView();
                    }

                }
            });

            playbackService.setOnShuffleChange(new PlaybackService.OnShuffleChangeInterface() {
                @Override
                public void onShuffleChange(Boolean shuffle) {
                    if (viewPager.getCurrentItem() == 1) {
                        now.shuffleView();
                    }

                }
            });

            playbackService.initProperty(activity.isReInit());
            activity.setReInit(false);
        }
//        else {
//            activity.bindServices();
//        }

        viewPager.setAdapter(mAdapter);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

//                Toast.makeText(activity, "pagescrolled", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onPageSelected(int position) {

//                Toast.makeText(activity, "page selected", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                if (viewPager.getCurrentItem() == 1) {
                    now.initVisual();
                }
                if (viewPager.getCurrentItem() == 2) {
                    que.refresh();
                }
//                Toast.makeText(activity, "scrollstate", Toast.LENGTH_SHORT).show();
            }
        });

        return navView;
    }

    @Override
    public void onResume() {
        super.onResume();



//        viewPager.setCurrentItem(1);

    }
}
