package com.acenosekai.antplay.fragment;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.acenosekai.antplay.R;
import com.acenosekai.antplay.adapter.FolderAdapter;
import com.acenosekai.antplay.adapter.QueueAdapter;
import com.acenosekai.antplay.sugar.BaseFragment;

/**
 * Created by Acenosekai on 12/27/2015.
 * Rock On
 */
public class QueueFragment extends BaseFragment {
    private View queueFragmentView;
    private QueueAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        this.queueFragmentView = inflater.inflate(R.layout.fragment_queue, container, false);

        RecyclerView contentList = (RecyclerView) queueFragmentView.findViewById(R.id.content_list);

        LinearLayoutManager llm = new LinearLayoutManager(activity);
        contentList.setLayoutManager(llm);
        contentList.setHasFixedSize(true);

        this.adapter = new QueueAdapter(activity);
        contentList.setAdapter(adapter);

//        nowPlayingView.findViewById(R.id.music_title).setSelected(true);
//        nowPlayingView.findViewById(R.id.music_description).setSelected(true);
//        nowPlayingView.findViewById(R.id.to_navigation).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                activity.changePage(new NavigationFragment());
//            }
//        });

//        ((Slider) nowPlayingView.findViewById(R.id.playback_seeker)).setValue(0, false);

        return queueFragmentView;
    }

    public void refresh() {
        adapter.notifyDataSetChanged();
    }
}
