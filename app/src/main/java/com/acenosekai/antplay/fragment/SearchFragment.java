package com.acenosekai.antplay.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.acenosekai.antplay.BaseActivity;
import com.acenosekai.antplay.R;
import com.acenosekai.antplay.adapter.SearchAdapter;
import com.acenosekai.antplay.realm.Album;
import com.acenosekai.antplay.realm.MusicFile;
import com.acenosekai.antplay.sugar.BaseFragment;
import com.acenosekai.antplay.sugar.FilesMenuBuilder;
import com.acenosekai.antplay.sugar.obj.Artist;
import com.rey.material.widget.EditText;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmResults;

/**
 * Created by Acenosekai on 12/12/2015.
 * Rock On
 */
public class SearchFragment extends BaseFragment {
    protected BaseFragment backFragment = new MainFragment();
    private Handler handler = new Handler();
    private View view;
    private Runnable runnable;
    private List<Object> objectList;
    private String searchText;
    private SearchAdapter adapter;

    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    @Override
    public BaseFragment getBackFragment() {
        return backFragment;
    }

    @Override
    public void setBackFragment(BaseFragment backFragment) {
        this.backFragment = backFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        this.view = inflater.inflate(R.layout.fragment_search, container, false);

        final RecyclerView contentList = (RecyclerView) view.findViewById(R.id.content_list);
        LinearLayoutManager llm = new LinearLayoutManager(activity);
        contentList.setLayoutManager(llm);
        contentList.setHasFixedSize(true);


        runnable = new Runnable() {
            @Override
            public void run() {
//                final ListView contentList = (ListView) view.findViewById(R.id.content_list);
                objectList = new ArrayList<>();
                if (searchText != null && !searchText.equals("")) {
                    //add music
                    RealmResults<MusicFile> musicFiles = app.getRealm().where(MusicFile.class).contains("title", searchText, false).findAll();
                    objectList.addAll(musicFiles);
                    //add artist
                    RealmResults<MusicFile> listAllMusic = app.getRealm().where(MusicFile.class).contains("artist", searchText, false).findAll();
                    listAllMusic.sort("artist");

                    List<Artist> artistList = new ArrayList<>();
                    String lastArtist = "";
                    for (MusicFile mf : listAllMusic) {
                        String artist = getActivity().getString(R.string.unknown_artist);
                        if (mf.getArtist() != null) {
                            artist = mf.getArtist();
                        }
                        if (!lastArtist.equals(artist)) {
                            Artist a = new Artist();
                            a.setName(artist);
                            artistList.add(a);
                        }
                        lastArtist = artist;
                    }
                    objectList.addAll(artistList);
                    RealmResults<Album> listAlbums = app.getRealm().where(Album.class).contains("name", searchText, false).findAll();
                    listAlbums.sort("name");
                    objectList.addAll(listAlbums);
                }


                adapter = new SearchAdapter(activity, objectList, searchText);
                contentList.setAdapter(adapter);


                activity.setOnMultiSelectModeChange(new BaseActivity.OnMultiSelectModeChange() {
                    @Override
                    public void onChange(boolean multiselect) {
                        if (multiselect) {
                            view.findViewById(R.id.nav_bar_multi).setVisibility(View.VISIBLE);
                        } else {
                            view.findViewById(R.id.nav_bar_multi).setVisibility(View.INVISIBLE);
                            adapter.notifyDataSetChanged();
                        }
                    }
                });
            }
        };

//        runnable.run();

        ((EditText) view.findViewById(R.id.search_edit)).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                searchText = s.toString();
                handler.removeCallbacks(runnable);
                handler.postDelayed(runnable, 500);
            }
        });

        ((EditText) view.findViewById(R.id.search_edit)).setText(searchText);

        view.findViewById(R.id.select_all).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.getMultiSelectHolder().clear();
                activity.getMultiSelectHolder().addAll(objectList);
                adapter.notifyDataSetChanged();
            }
        });

        view.findViewById(R.id.un_select_all).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.getMultiSelectHolder().clear();
                adapter.notifyDataSetChanged();
            }
        });

        view.findViewById(R.id.menu_multi_select).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new FilesMenuBuilder(activity).buildMenuFiles(v, activity.getMultiSelectHolder());
            }
        });

        return view;
    }

}
