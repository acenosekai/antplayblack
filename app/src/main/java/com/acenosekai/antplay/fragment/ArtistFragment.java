package com.acenosekai.antplay.fragment;

import android.widget.TextView;

import com.acenosekai.antplay.R;
import com.acenosekai.antplay.realm.MusicFile;
import com.acenosekai.antplay.sugar.BaseFilesFragment;
import com.acenosekai.antplay.sugar.BaseFragment;
import com.acenosekai.antplay.sugar.obj.Artist;

import net.soroushjavdan.customfontwidgets.CTextView;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmResults;

/**
 * Created by Acenosekai on 12/10/2015.
 * Rock On
 */
public class ArtistFragment extends BaseFilesFragment {

    protected BaseFragment backFragment = new MainFragment();

    @Override
    public BaseFragment getBackFragment() {
        return backFragment;
    }

    @Override
    public void setBackFragment(BaseFragment backFragment) {
        this.backFragment = backFragment;
    }


    @Override
    public String getTitle() {
        return getString(R.string.fragment_artist_title);
    }

    @Override
    public List<?> getListObject() {
        if (activity.getListArtist() == null) {
            RealmResults<MusicFile> listAllMusic = app.getRealm().where(MusicFile.class).findAll();
            listAllMusic.sort("artist");
            List<Artist> artistList = new ArrayList<>();
            String lastArtist = "";
            for (MusicFile mf : listAllMusic) {
                String artist = getActivity().getString(R.string.unknown_artist);

                if (mf.getArtist() != null) {
                    artist = mf.getArtist();
                }
                if (!lastArtist.equals(artist)) {
                    Artist a = new Artist();
                    a.setName(artist);
                    artistList.add(a);
                }
                lastArtist = artist;
            }
            activity.setListArtist(artistList);
        }
        return activity.getListArtist();
    }

    @Override
    public void onResume() {
        super.onResume();
        ((CTextView) filesView.findViewById(R.id.parent_files)).setText(getActivity().getString(R.string.all_artist));
        filesView.findViewById(R.id.parent_files).setSelected(true);
    }
}
