package com.acenosekai.antplay.fragment;

import android.graphics.Point;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.CompoundButton;

import com.acenosekai.antplay.App;
import com.acenosekai.antplay.R;
import com.acenosekai.antplay.adapter.EqPresetAdapter;
import com.acenosekai.antplay.adapter.EqualizerAdapter;
import com.acenosekai.antplay.realm.ParamEQ;
import com.acenosekai.antplay.sugar.BaseFragment;
import com.rey.material.app.Dialog;

import net.soroushjavdan.customfontwidgets.CTextView;

/**
 * Created by Acenosekai on 1/4/2016.
 * Rock On
 */
public class EqualizerFragment extends BaseFragment {
    protected BaseFragment backFragment = new MainFragment();
    private View eqView;
    private RecyclerView contentList;
    private EqualizerAdapter adapter;

    @Override
    public BaseFragment getBackFragment() {
        return backFragment;
    }

    @Override
    public void setBackFragment(BaseFragment backFragment) {
        this.backFragment = backFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        this.eqView = inflater.inflate(R.layout.fragment_equalizer, container, false);
        CTextView ctv = (CTextView) eqView.findViewById(R.id.fragment_title);
        ctv.setText(R.string.fragment_equalizer_title);
        SwitchCompat fragmentSwitch = (SwitchCompat) eqView.findViewById(R.id.fragment_switch);
//        if (useFragmentSwitch()) {
//
        fragmentSwitch.setChecked(app.getRegistry(App.PEAEQ).equals("1"));
        fragmentSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    app.setRegistry(App.PEAEQ, "1");
                } else {
                    app.setRegistry(App.PEAEQ, "0");
                }
            }
        });
//        } else {
//            fragmentSwitch.setVisibility(View.GONE);
//        }
//
        this.contentList = (RecyclerView) eqView.findViewById(R.id.content_list);
        LinearLayoutManager llm = new LinearLayoutManager(activity);
        contentList.setLayoutManager(llm);
        contentList.setHasFixedSize(true);
//

        this.adapter = new EqualizerAdapter(activity);
        contentList.setAdapter(adapter);


        eqView.findViewById(R.id.preset_show).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Point size = new Point();
                WindowManager w = activity.getWindowManager();
                w.getDefaultDisplay().getSize(size);
                View dialogView = activity.getLayoutInflater().inflate(R.layout.dialog_list_basic, null);
                RecyclerView recyclerView = (RecyclerView) dialogView.findViewById(R.id.string_list);
                LinearLayoutManager llm = new LinearLayoutManager(activity);
                recyclerView.setLayoutManager(llm);
                recyclerView.setHasFixedSize(true);


                final Dialog presetDialog = new Dialog(activity);

                EqPresetAdapter adapter = new EqPresetAdapter(activity, app.getRealm().where(ParamEQ.class).findAll(), presetDialog);
                recyclerView.setAdapter(adapter);

                presetDialog.applyStyle(R.style.Material_App_Dialog)
                        .title(R.string.dialog_eq_preset_title)
                        .contentView(dialogView)
                        .cancelable(true)
                        .canceledOnTouchOutside(true)
                        .layoutParams(((int) (size.x * 0.8)), ((int) (size.y * 0.8))).show();

            }
        });

        return eqView;
    }
}
