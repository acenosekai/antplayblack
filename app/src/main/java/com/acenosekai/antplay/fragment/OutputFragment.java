package com.acenosekai.antplay.fragment;

import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.Spinner;

import com.acenosekai.antplay.App;
import com.acenosekai.antplay.R;
import com.acenosekai.antplay.sugar.BaseEffectFragment;
import com.acenosekai.antplay.sugar.obj.SliderItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Acenosekai on 1/2/2016.
 * Rock On
 */
public class OutputFragment extends BaseEffectFragment {
    @Override
    public int getLayout() {
        return R.layout.fragment_output;
    }

    @Override
    public List<SliderItem> getSliderItemList() {
        List<SliderItem> sliderItemList = new ArrayList<>();


        SliderItem buffer = new SliderItem();
        buffer.setLabel(getString(R.string.output_buffer));
        buffer.setSuffix(getString(R.string.output_ms));
        buffer.setMin(10);
        buffer.setMax(5000);
        buffer.setStep(499);
        buffer.setValue(app.getRegistryFloat(App.CONFIG_BUFFER));
        buffer.setOnPositionChangeListener(new SliderItem.OnPositionChangeListener() {
            @Override
            public void onPositionChanged(float value) {
                app.setRegistry(App.CONFIG_BUFFER, String.valueOf(value));
            }
        });
        sliderItemList.add(buffer);

        SliderItem src = new SliderItem();
        src.setLabel(getString(R.string.output_src));
        src.setMax(5);
        src.setStep(5);
        src.setValue(app.getRegistryFloat(App.CONFIG_SRC));
        src.setOnPositionChangeListener(new SliderItem.OnPositionChangeListener() {
            @Override
            public void onPositionChanged(float value) {
                app.setRegistry(App.CONFIG_SRC, String.valueOf(value));
            }
        });
        sliderItemList.add(src);


        return sliderItemList;
    }

    @Override
    public int getFragmentTitle() {
        return R.string.fragment_output_title;
    }

    @Override
    public boolean useFragmentSwitch() {
        return false;
    }

    @Override
    public int getFragmentSwitchKey() {
        return 0;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View baseView = super.onCreateView(inflater, container, savedInstanceState);
        Spinner sampleRate = (Spinner) baseView.findViewById(R.id.output_sample_spinner);

        final String[] sampleRates = {"11025", "22050", "44100", "48000", "96000", "192000"};
        ArrayAdapter<String> adapter = new ArrayAdapter<>(activity, android.R.layout.simple_spinner_item, sampleRates);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sampleRate.setAdapter(adapter);
        int i = 0;
        for (String sample : sampleRates) {
            if (sample.equals(app.getRegistry(App.CONFIG_SAMPLE_RATE))) {
                sampleRate.setSelection(i);
                break;
            }
            i++;
        }

        sampleRate.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                app.setRegistry(App.CONFIG_SAMPLE_RATE, sampleRates[position]);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        SwitchCompat floatPoint = (SwitchCompat) baseView.findViewById(R.id.output_float_switch);
        floatPoint.setChecked(app.getRegistry(App.CONFIG_FLOATDSP).equals("1"));

        floatPoint.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    app.setRegistry(App.CONFIG_FLOATDSP, "1");
                } else {
                    app.setRegistry(App.CONFIG_FLOATDSP, "0");
                }
            }
        });

        return baseView;
    }
}
