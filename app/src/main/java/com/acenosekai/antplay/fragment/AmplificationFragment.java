package com.acenosekai.antplay.fragment;

import com.acenosekai.antplay.App;
import com.acenosekai.antplay.R;
import com.acenosekai.antplay.sugar.BaseEffectFragment;
import com.acenosekai.antplay.sugar.obj.SliderItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Acenosekai on 1/1/2016.
 * Rock On
 */
public class AmplificationFragment extends BaseEffectFragment {
    @Override
    public List<SliderItem> getSliderItemList() {
        List<SliderItem> sliderItemList = new ArrayList<>();

        SliderItem ampSlider = new SliderItem();
        ampSlider.setLabel(getString(R.string.fragment_amplification_label));
        ampSlider.setSuffix(getString(R.string.suffix_db));
        ampSlider.setValue(app.getRegistryFloat(App.VOLUME));
        ampSlider.setMin(-60);
        ampSlider.setMax(15);
        ampSlider.setStep(150);
        ampSlider.setOnPositionChangeListener(new SliderItem.OnPositionChangeListener() {
            @Override
            public void onPositionChanged(float value) {
                app.setRegistry(App.VOLUME, String.valueOf(value));
            }
        });

        sliderItemList.add(ampSlider);
        return sliderItemList;
    }

    @Override
    public int getFragmentTitle() {
        return R.string.fragment_amplification_title;
    }

    @Override
    public boolean useFragmentSwitch() {
        return false;
    }

    @Override
    public int getFragmentSwitchKey() {
        return 0;
    }
}
