package com.acenosekai.antplay.fragment;

import android.view.View;
import android.widget.TextView;

import com.acenosekai.antplay.R;
import com.acenosekai.antplay.realm.PlayList;
import com.acenosekai.antplay.sugar.BaseFilesFragment;
import com.acenosekai.antplay.sugar.BaseFragment;
import com.acenosekai.antplay.sugar.FilesMenuBuilder;

import net.soroushjavdan.customfontwidgets.CTextView;

import java.util.List;

/**
 * Created by Acenosekai on 12/17/2015.
 * Rock On
 */
public class PlayListBaseFragment extends BaseFilesFragment {
    protected BaseFragment backFragment = new MainFragment();

    @Override
    public BaseFragment getBackFragment() {
        return backFragment;
    }

    @Override
    public void setBackFragment(BaseFragment backFragment) {
        this.backFragment = backFragment;
    }


    @Override
    public String getTitle() {
        return getString(R.string.fragment_play_list_title);
    }

    @Override
    public List<?> getListObject() {
        return app.getRealm().where(PlayList.class).notEqualTo("name", "__defaultPlaylist").findAll();
    }

    @Override
    public void onResume() {
        super.onResume();
        ((CTextView) filesView.findViewById(R.id.parent_files)).setText(getActivity().getString(R.string.all_playlist));
        filesView.findViewById(R.id.parent_files).setSelected(true);
    }

    public Menu getMenu() {
        return new Menu() {
            @Override
            public void build(View v) {
                new FilesMenuBuilder(activity).buildMenuPlaylist(v, activity.getMultiSelectHolder());
            }
        };
    }

}
