package com.acenosekai.antplay.fragment;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.acenosekai.antplay.BaseActivity;
import com.acenosekai.antplay.R;
import com.acenosekai.antplay.adapter.FolderAdapter;
import com.acenosekai.antplay.realm.MediaLibrary;
import com.acenosekai.antplay.realm.MusicFile;
import com.acenosekai.antplay.sugar.BaseFragment;
import com.acenosekai.antplay.sugar.FilesMenuBuilder;
import com.acenosekai.antplay.sugar.obj.Folder;

import net.soroushjavdan.customfontwidgets.CTextView;

import org.apache.commons.lang3.StringUtils;

import java.util.List;

import io.realm.RealmResults;

/**
 * Created by Acenosekai on 12/9/2015.
 * Rock On
 */
public class FolderFragment extends BaseFragment {
    protected BaseFragment backFragment = new MainFragment();
    private FolderAdapter adapter;
    private Folder currentFolder;
    private View folderView;
    private RecyclerView contentList;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        this.folderView = inflater.inflate(R.layout.fragment_files_up, container, false);
        CTextView ctv = (CTextView) folderView.findViewById(R.id.fragment_title);
        ctv.setText(R.string.fragment_folder_title);


        if (activity.getRootTree() == null || activity.getRootTree().getChilds().isEmpty()) {
            RealmResults<MediaLibrary> libraries = app.getRealm().where(MediaLibrary.class).findAll();
            Folder rootTree = new Folder();
            rootTree.setPath("/");
            rootTree.setName(activity.getString(R.string.root_folder));
            rootTree.setDirectory(true);

            for (MediaLibrary lib : libraries) {
                final Folder ft = new Folder();
                ft.setPath(lib.getPath());
                ft.setName(lib.getPath());
                ft.setDirectory(true);
                ft.setParent(rootTree);
                RealmResults<MusicFile> musicFiles = app.getRealm().where(MusicFile.class).beginsWith("path", lib.getPath()).findAll();
                for (MusicFile mf : musicFiles) {
                    generateFileTree(ft, mf);
                }
                rootTree.getChilds().add(ft);
            }
            activity.setRootTree(rootTree);
        }

        setCurrentFolder(activity.getRootTree());

        this.contentList = (RecyclerView) folderView.findViewById(R.id.content_list);
        LinearLayoutManager llm = new LinearLayoutManager(activity);
        contentList.setLayoutManager(llm);
        contentList.setHasFixedSize(true);

        generateAdapter(activity.getRootTree().getChilds());

        folderView.findViewById(R.id.directory_up).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Folder parent = currentFolder.getParent();
                if (parent != null) {
                    generateAdapter(parent.getChilds());
                    setCurrentFolder(parent);
                }

            }
        });

        folderView.findViewById(R.id.select_all).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.getMultiSelectHolder().clear();
                activity.getMultiSelectHolder().addAll(currentFolder.getChilds());
                adapter.notifyDataSetChanged();
            }
        });

        folderView.findViewById(R.id.un_select_all).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.getMultiSelectHolder().clear();
                adapter.notifyDataSetChanged();
            }
        });

        folderView.findViewById(R.id.menu_multi_select).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new FilesMenuBuilder(activity).buildMenuFiles(v, activity.getMultiSelectHolder());
            }
        });

        return folderView;
    }

    public void generateAdapter(List<Folder> folders) {
        this.adapter = new FolderAdapter(activity, folders, FolderFragment.this);
        contentList.setAdapter(adapter);

        activity.setOnMultiSelectModeChange(new BaseActivity.OnMultiSelectModeChange() {
            @Override
            public void onChange(boolean multiSelect) {
                if (multiSelect) {
                    folderView.findViewById(R.id.nav_bar_multi).setVisibility(View.VISIBLE);
                } else {
                    folderView.findViewById(R.id.nav_bar_multi).setVisibility(View.INVISIBLE);
                    adapter.notifyDataSetChanged();
                }
            }
        });
    }

    public void setCurrentFolder(Folder currentFolder) {
        this.currentFolder = currentFolder;
        ((CTextView) folderView.findViewById(R.id.parent_files)).setText(currentFolder.getName());
        folderView.findViewById(R.id.parent_files).setSelected(true);
    }

    @Override
    public BaseFragment getBackFragment() {
        return backFragment;
    }

    @Override
    public void setBackFragment(BaseFragment backFragment) {
        this.backFragment = backFragment;
    }

    public void generateFileTree(Folder ft, MusicFile mf) {
        String[] arrParent = StringUtils.split(ft.getPath(), "\\/");
        String[] arrStr = StringUtils.split(mf.getPath(), "\\/");
        if (arrStr.length > arrParent.length) {
            boolean needAdd = false;
            Folder chTree = isChildExist(ft, ft.getPath() + "/" + arrStr[arrParent.length]);
            if (chTree == null) {
                needAdd = true;
                chTree = new Folder();
                chTree.setName(arrStr[arrParent.length]);
                chTree.setPath(ft.getPath() + "/" + arrStr[arrParent.length]);
                chTree.setParent(ft);
            }
            if (arrStr.length - arrParent.length > 1) {
                chTree.setDirectory(true);
                generateFileTree(chTree, mf);
            } else {
                chTree.setDirectory(false);
                chTree.setName(mf.getTitle());
            }
            if (needAdd) {
                ft.getChilds().add(chTree);
            }
        }
    }

    private Folder isChildExist(Folder ft, String path) {
        List<Folder> chList = ft.getChilds();
        for (Folder ftc : chList) {
            if (ftc.getPath().equals(path)) {
                return ftc;
            }
        }

        return null;
    }


}
