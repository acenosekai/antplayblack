package com.acenosekai.antplay.fragment;

import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.acenosekai.antplay.App;
import com.acenosekai.antplay.R;
import com.acenosekai.antplay.realm.MediaLibrary;
import com.acenosekai.antplay.services.PlaybackService;
import com.acenosekai.antplay.sugar.BaseFragment;
import com.acenosekai.antplay.sugar.widget.SquareImageView;
import com.mikepenz.community_material_typeface_library.CommunityMaterial;
import com.mikepenz.iconics.view.IconicsImageView;
import com.rey.material.widget.Slider;

import net.soroushjavdan.customfontwidgets.CTextView;


public class NowplayingFragment extends BaseFragment {
    private View nowPlayingView;
    private PlaybackService playbackService;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        this.nowPlayingView = inflater.inflate(R.layout.fragment_nowplaying, container, false);


        nowPlayingView.findViewById(R.id.music_title).setSelected(true);
        nowPlayingView.findViewById(R.id.music_description).setSelected(true);

        ((Slider) nowPlayingView.findViewById(R.id.playback_seeker)).setValue(0, false);

        return nowPlayingView;
    }

    @Override
    public void onResume() {
        super.onResume();

        Point size = new Point();
        activity.getWindowManager().getDefaultDisplay().getSize(size);

        this.playbackService = activity.getPlaybackService();
        if (playbackService != null) {

            ((Slider) nowPlayingView.findViewById(R.id.playback_seeker)).setOnPositionChangeListener(new Slider.OnPositionChangeListener() {
                @Override
                public void onPositionChanged(Slider view, boolean fromUser, float oldPos, float newPos, int oldValue, int newValue) {
                    if (fromUser) {
                        playbackService.setSecondsPosition(newValue);
                        try {
                            initVisual();
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                    }
                }
            });

            nowPlayingView.findViewById(R.id.play).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (playbackService.isPlaying()) {
                        playbackService.playbackPause();
                    } else {
                        playbackService.playbackPlay(true);
                    }
                }
            });

            nowPlayingView.findViewById(R.id.next).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    playbackService.playbackNext(true);
                }
            });

            nowPlayingView.findViewById(R.id.prev).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    playbackService.playbackPrev();
                }
            });

            nowPlayingView.findViewById(R.id.repeat).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    switch (Integer.valueOf(app.getRegistry(App.REPEAT))) {
                        case PlaybackService.Repeat.NO_REPEAT:
                            app.setRegistry(App.REPEAT, String.valueOf(PlaybackService.Repeat.REPEAT_ALL));
                            break;
                        case PlaybackService.Repeat.REPEAT_ALL:
                            app.setRegistry(App.REPEAT, String.valueOf(PlaybackService.Repeat.REPEAT_ONE));
                            break;
                        case PlaybackService.Repeat.REPEAT_ONE:
                            app.setRegistry(App.REPEAT, String.valueOf(PlaybackService.Repeat.NO_REPEAT));
                            break;
                    }
                }
            });

            nowPlayingView.findViewById(R.id.shuffle).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    switch (Integer.valueOf(app.getRegistry(App.SHUFFLE))) {
                        case 0:
                            app.setRegistry(App.SHUFFLE, "1");
                            break;
                        case 1:
                            app.setRegistry(App.SHUFFLE, "0");
                            break;
                    }

                }
            });
            initVisual();
            initVisual();
        } else {

        }
    }

    public void shuffleView() {
        IconicsImageView shuffleIcon = (IconicsImageView) nowPlayingView.findViewById(R.id.shuffle);
        if (app.getRegistry(App.SHUFFLE).equals("1")) {
            shuffleIcon.setColor(ContextCompat.getColor(activity, R.color.colorWhite));
        } else {
            shuffleIcon.setColor(ContextCompat.getColor(activity, R.color.colorSemiBlack));
        }
    }

    public void repeatView() {
        IconicsImageView repeatIcon = (IconicsImageView) nowPlayingView.findViewById(R.id.repeat);
        switch (Integer.valueOf(app.getRegistry(App.REPEAT))) {
            case PlaybackService.Repeat.NO_REPEAT:
                repeatIcon.setIcon(CommunityMaterial.Icon.cmd_repeat);
                repeatIcon.setColor(ContextCompat.getColor(activity, R.color.colorSemiBlack));
                break;
            case PlaybackService.Repeat.REPEAT_ALL:
                repeatIcon.setIcon(CommunityMaterial.Icon.cmd_repeat);
                repeatIcon.setColor(ContextCompat.getColor(activity, R.color.colorWhite));
                break;
            case PlaybackService.Repeat.REPEAT_ONE:
                repeatIcon.setIcon(CommunityMaterial.Icon.cmd_repeat_once);
                repeatIcon.setColor(ContextCompat.getColor(activity, R.color.colorWhite));
                break;
        }
    }

    public void playMode() {
        IconicsImageView miv = (IconicsImageView) nowPlayingView.findViewById(R.id.play);
        miv.setIcon(CommunityMaterial.Icon.cmd_pause);
    }

    public void pauseMode() {
        IconicsImageView miv = (IconicsImageView) nowPlayingView.findViewById(R.id.play);
        miv.setIcon(CommunityMaterial.Icon.cmd_play);
    }

    public void playingVisual() {
        if (playbackService != null) {
            ((CTextView) nowPlayingView.findViewById(R.id.position)).setText(playbackService.getPositionTime());
            ((Slider) nowPlayingView.findViewById(R.id.playback_seeker)).setValue((float) playbackService.getSecondsPosition(), false);
        }
    }

    public void initVisual() {
        if (playbackService != null) {
            try {
                if (playbackService.isPlaying()) {
                    playMode();
                } else {
                    pauseMode();
                }

                repeatView();
                shuffleView();

                ((CTextView) nowPlayingView.findViewById(R.id.music_title)).setText(playbackService.getMusicFile().getTitle());
                ((CTextView) nowPlayingView.findViewById(R.id.music_description)).setText(playbackService.getMusicFile().getArtist() + " - " + playbackService.getMusicFile().getAlbum().getName());
                ((CTextView) nowPlayingView.findViewById(R.id.total)).setText(playbackService.getTotalTime());
                ((Slider) nowPlayingView.findViewById(R.id.playback_seeker)).setValueRange(0, playbackService.getSecondsTotal(), false);
                SquareImageView coverView = (SquareImageView) nowPlayingView.findViewById(R.id.cover);

                if (playbackService.getBitMapCover() != null) {//
                    coverView.setImageBitmap(playbackService.getBitMapCover());
                } else {
                    coverView.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.cd));
                }
            } catch (Exception e) {
                if (app.getRealm().where(MediaLibrary.class).findAll().isEmpty()) {
                    Toast.makeText(getActivity(), "go to library", Toast.LENGTH_SHORT).show();
                    activity.changePage(new LibraryFragment());
                } else {
                    Toast.makeText(getActivity(), "go to files", Toast.LENGTH_SHORT).show();
                    activity.changePage(new FolderFragment());
                }
            }

        }

    }


}
