package com.acenosekai.antplay.fragment;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.acenosekai.antplay.BaseActivity;
import com.acenosekai.antplay.R;
import com.acenosekai.antplay.adapter.LibraryAdapter;
import com.acenosekai.antplay.realm.MediaLibrary;
import com.acenosekai.antplay.sugar.BaseFragment;
import com.acenosekai.antplay.sugar.LibraryGenerator;

import net.soroushjavdan.customfontwidgets.CTextView;

import io.realm.RealmResults;

public class LibraryFragment extends BaseFragment {

    protected BaseFragment backFragment = new MainFragment();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View libraryView = inflater.inflate(R.layout.fragment_library, container, false);
        libraryView.findViewById(R.id.nav_directory_add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((BaseActivity) getActivity()).changePage(new DirectoryChooserFragment());
            }
        });
        ((BaseActivity) getActivity()).setRootTree(null);
        ((BaseActivity) getActivity()).setListAlbums(null);
        ((BaseActivity) getActivity()).setListArtist(null);
        RealmResults<MediaLibrary> mediaLibrariesResult = app.getRealm().where(MediaLibrary.class).findAll();

        final RecyclerView contentList = (RecyclerView) libraryView.findViewById(R.id.library_list);
        LinearLayoutManager llm = new LinearLayoutManager(activity);
        contentList.setLayoutManager(llm);
        contentList.setHasFixedSize(true);

        LibraryAdapter adapter = new LibraryAdapter(activity, mediaLibrariesResult);
        contentList.setAdapter(adapter);

//        final ListView libraryListView = (ListView) libraryView.findViewById(R.id.library_list);
//        libraryListView.setAdapter(new LibraryAdapter(getActivity(), R.layout.item_library, mediaLibrariesResult));

        CTextView ctv = (CTextView) libraryView.findViewById(R.id.fragment_title);
        ctv.setText(R.string.fragment_library_title);
        libraryView.findViewById(R.id.nav_directory_reload).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LibraryGenerator libraryGenerator = new LibraryGenerator(app);
                libraryGenerator.reload();
                RealmResults<MediaLibrary> mediaLibrariesResult = app.getRealm().where(MediaLibrary.class).findAll();
                LibraryAdapter adapter = new LibraryAdapter(activity, mediaLibrariesResult);
                contentList.setAdapter(adapter);
            }
        });
        return libraryView;
    }

    @Override
    public BaseFragment getBackFragment() {
        return backFragment;
    }

    @Override
    public void setBackFragment(BaseFragment backFragment) {
        this.backFragment = backFragment;
    }

}
