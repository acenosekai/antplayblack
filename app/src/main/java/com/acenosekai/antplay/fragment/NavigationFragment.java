package com.acenosekai.antplay.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.acenosekai.antplay.BaseActivity;
import com.acenosekai.antplay.R;
import com.acenosekai.antplay.adapter.NavigationAdapter;
import com.acenosekai.antplay.objView.DividerNavigation;
import com.acenosekai.antplay.objView.Navigation;
import com.acenosekai.antplay.objView.SwitchNavigation;
import com.acenosekai.antplay.sugar.BaseFragment;

import java.util.ArrayList;
import java.util.List;

public class NavigationFragment extends BaseFragment {


    protected BaseFragment backFragment = new MainFragment();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        // Inflate the layout for this fragment
        View navView = inflater.inflate(R.layout.fragment_navigation, container, false);
//        CTextView ctv = (CTextView) navView.findViewById(R.id.fragment_title);
//        ctv.setText(R.string.fragment_navigation_title);

        ListView navListContainer = (ListView) navView.findViewById(R.id.navigation_list);

        List<Navigation> listNav = new ArrayList<>();
        listNav.add(new Navigation().setName("Files").setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((BaseActivity) getActivity()).changePage(new FolderFragment());
            }
        }));

        listNav.add(new Navigation().setName("Artist").setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((BaseActivity) getActivity()).changePage(new ArtistFragment());
            }
        }));

        listNav.add(new Navigation().setName("Album").setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((BaseActivity) getActivity()).changePage(new AlbumFragment());
            }
        }));

        listNav.add(new Navigation().setName("Search").setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((BaseActivity) getActivity()).changePage(new SearchFragment());
            }
        }));
        listNav.add(new Navigation().setName("Playlist").setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((BaseActivity) getActivity()).changePage(new PlayListBaseFragment());
            }
        }));
        listNav.add(new DividerNavigation());

        //AmplificationFragment
        listNav.add(new SwitchNavigation().setName("Crossfade"));
        listNav.add(new SwitchNavigation().setName("Repeat"));
        listNav.add(new SwitchNavigation().setName("Shuffle"));
        listNav.add(new DividerNavigation());
        listNav.add(new Navigation().setName("Amplification").setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((BaseActivity) getActivity()).changePage(new AmplificationFragment());

            }
        }));
        listNav.add(new Navigation().setName("Freeverb").setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((BaseActivity) getActivity()).changePage(new FreeverbFragment());

            }
        }));

        listNav.add(new Navigation().setName("Dynamic Amp").setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((BaseActivity) getActivity()).changePage(new DampFragment());

            }
        }));


        listNav.add(new Navigation().setName("Compressor").setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((BaseActivity) getActivity()).changePage(new CompressorFragment());
            }
        }));
        listNav.add(new Navigation().setName("Equalizer").setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((BaseActivity) getActivity()).changePage(new EqualizerFragment());
            }
        }));
        listNav.add(new DividerNavigation());
        listNav.add(new Navigation().setName("Library").setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((BaseActivity) getActivity()).changePage(new LibraryFragment());
            }
        }));
        listNav.add(new Navigation().setName("Output").setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((BaseActivity) getActivity()).changePage(new OutputFragment());
            }
        }));

        navListContainer.setDividerHeight(0);
        navListContainer.setDivider(null);
        NavigationAdapter navAdapter = new NavigationAdapter(getActivity(), R.layout.navigation_link, listNav);
        navListContainer.setAdapter(navAdapter);

        return navView;
    }

    @Override
    public BaseFragment getBackFragment() {
        return backFragment;
    }

    @Override
    public void setBackFragment(BaseFragment backFragment) {
        this.backFragment = backFragment;
    }


}
