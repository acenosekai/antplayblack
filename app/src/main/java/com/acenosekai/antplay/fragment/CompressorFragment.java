package com.acenosekai.antplay.fragment;

import com.acenosekai.antplay.App;
import com.acenosekai.antplay.R;
import com.acenosekai.antplay.sugar.BaseEffectFragment;
import com.acenosekai.antplay.sugar.obj.SliderItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Acenosekai on 1/2/2016.
 * Rock On
 */
public class CompressorFragment extends BaseEffectFragment {
    @Override
    public List<SliderItem> getSliderItemList() {
        List<SliderItem> sliderItemList = new ArrayList<>();
////
        SliderItem gain = new SliderItem();

        gain.setLabel(getString(R.string.compressor_gain));
        gain.setValue(app.getRegistryFloat(App.COMPRESSOR_GAIN));
        gain.setSuffix(getString(R.string.compressor_db));
        gain.setMin(-60);
        gain.setMax(15);
        gain.setStep(150);
        gain.setOnPositionChangeListener(new SliderItem.OnPositionChangeListener() {
            @Override
            public void onPositionChanged(float value) {
                app.setRegistry(App.COMPRESSOR_GAIN, String.valueOf(value));
            }
        });
        sliderItemList.add(gain);

        SliderItem threshold = new SliderItem();
        threshold.setLabel(getString(R.string.compressor_threshold));
        threshold.setValue(app.getRegistryFloat(App.COMPRESSOR_THRESHOLD));
        threshold.setSuffix(getString(R.string.compressor_db));
        threshold.setMin(-60);
        threshold.setMax(15);
        threshold.setStep(150);
        threshold.setOnPositionChangeListener(new SliderItem.OnPositionChangeListener() {
            @Override
            public void onPositionChanged(float value) {
                app.setRegistry(App.COMPRESSOR_THRESHOLD, String.valueOf(value));
            }
        });
        sliderItemList.add(threshold);


        SliderItem ratio = new SliderItem();
        ratio.setLabel(getString(R.string.compressor_ratio));
        ratio.setValue(app.getRegistryFloat(App.COMPRESSOR_RATIO));
        ratio.setMin(1);
        ratio.setMax(10);
        ratio.setStep(98);
        ratio.setOnPositionChangeListener(new SliderItem.OnPositionChangeListener() {
            @Override
            public void onPositionChanged(float value) {
                app.setRegistry(App.COMPRESSOR_RATIO, String.valueOf(value));
            }
        });
        sliderItemList.add(ratio);

        SliderItem attack = new SliderItem();
        attack.setLabel(getString(R.string.compressor_attack));
        attack.setSuffix(getString(R.string.compressor_ms));
        attack.setValue(app.getRegistryFloat(App.COMPRESSOR_ATTACK));
        attack.setMin(10);
        attack.setMax(1000);
        attack.setStep(99);
        attack.setOnPositionChangeListener(new SliderItem.OnPositionChangeListener() {
            @Override
            public void onPositionChanged(float value) {
                app.setRegistry(App.COMPRESSOR_ATTACK, String.valueOf(value));
            }
        });
        sliderItemList.add(attack);

        SliderItem release = new SliderItem();
        release.setLabel(getString(R.string.compressor_release));
        release.setSuffix(getString(R.string.compressor_ms));
        release.setValue(app.getRegistryFloat(App.COMPRESSOR_RELEASE));
        release.setMin(10);
        release.setMax(5000);
        release.setStep(499);
        release.setOnPositionChangeListener(new SliderItem.OnPositionChangeListener() {
            @Override
            public void onPositionChanged(float value) {
                app.setRegistry(App.COMPRESSOR_RELEASE, String.valueOf(value));
            }
        });
        sliderItemList.add(release);


        return sliderItemList;
    }

    @Override
    public int getFragmentTitle() {
        return R.string.fragment_compressor_title;
    }

    @Override
    public boolean useFragmentSwitch() {
        return true;
    }

    @Override
    public int getFragmentSwitchKey() {
        return App.COMPRESSOR;
    }
}
