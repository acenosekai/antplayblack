package com.acenosekai.antplay.fragment;


import android.widget.TextView;

import com.acenosekai.antplay.R;
import com.acenosekai.antplay.realm.Album;
import com.acenosekai.antplay.sugar.BaseFilesFragment;
import com.acenosekai.antplay.sugar.BaseFragment;

import net.soroushjavdan.customfontwidgets.CTextView;

import java.util.List;

import io.realm.RealmResults;

/**
 * Created by Acenosekai on 12/12/2015.
 * Rock On
 */
public class AlbumFragment extends BaseFilesFragment {
    protected BaseFragment backFragment = new MainFragment();

    @Override
    public BaseFragment getBackFragment() {
        return backFragment;
    }

    @Override
    public void setBackFragment(BaseFragment backFragment) {
        this.backFragment = backFragment;
    }

    @Override
    public String getTitle() {
        return getString(R.string.fragment_album_title);
    }

    @Override
    public List<?> getListObject() {
        if (activity.getListAlbums() == null) {
            RealmResults<Album> listAlbums = app.getRealm().where(Album.class).findAll();
            listAlbums.sort("name");
            activity.setListAlbums(listAlbums);
        }
        return activity.getListAlbums();
    }

    @Override
    public void onResume() {
        super.onResume();
        ((CTextView) filesView.findViewById(R.id.parent_files)).setText(getActivity().getString(R.string.all_album));
        filesView.findViewById(R.id.parent_files).setSelected(true);
    }
}
