package com.acenosekai.antplay.fragment;


import android.os.Bundle;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.acenosekai.antplay.BaseActivity;
import com.acenosekai.antplay.R;
import com.acenosekai.antplay.adapter.DirectoryAdapter;
import com.acenosekai.antplay.sugar.BaseFragment;
import com.acenosekai.antplay.sugar.LibraryGenerator;

import net.soroushjavdan.customfontwidgets.CTextView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class DirectoryChooserFragment extends BaseFragment {

    protected BaseFragment backFragment = new LibraryFragment();
    private String selectedPath = Environment.getExternalStorageDirectory().getPath();
    private View directoryChooserView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        this.directoryChooserView = inflater.inflate(R.layout.fragment_directory_chooser, container, false);
        directoryChooserView.findViewById(R.id.directory_up).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    up(selectedPath);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        CTextView ctv = (CTextView) directoryChooserView.findViewById(R.id.fragment_title);
        ctv.setText(R.string.fragment_directory_title);
        try {
            down(selectedPath);
        } catch (Exception e) {
            e.printStackTrace();
        }

        directoryChooserView.findViewById(R.id.directory_select).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                LibraryGenerator libGenerator = new LibraryGenerator(app);
                if (libGenerator.add(selectedPath)) {
                    ((BaseActivity) getActivity()).changePage(new LibraryFragment());
                } else {
                    Toast.makeText(getActivity(), "This folder or parent folder is already included", Toast.LENGTH_SHORT).show();
                }

            }
        });

        return directoryChooserView;
    }

    private void up(String path) throws Exception {
        File f = new File(path);
        down(f.getParent());
    }

    private void down(final String path) throws Exception {
        this.selectedPath = path;
        CTextView dirSelectedText = (CTextView) directoryChooserView.findViewById(R.id.directory_selected);
        directoryChooserView.findViewById(R.id.directory_selected).setSelected(true);
        String selectedText = path;
        dirSelectedText.setText(selectedText);
        ListView directoryList = (ListView) directoryChooserView.findViewById(R.id.directory_list);
        directoryList.setAdapter(new DirectoryAdapter(getActivity(), R.layout.item_directory, generateChild(path)));
        directoryList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                try {
                    CTextView ctv = (CTextView) view.findViewById(R.id.directory_name);
                    if (path.equals("/")) {
                        down(path + ctv.getText().toString());
                    } else {
                        down(path + "/" + ctv.getText().toString());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private List<String> generateChild(String path) {
        List<String> childList = new ArrayList<>();
        File f = new File(path);
        for (File child : f.listFiles()) {
            if (child.isDirectory()) {
                childList.add(child.getName());
            }
        }
        return childList;
    }

    @Override
    public BaseFragment getBackFragment() {
        return backFragment;
    }

    @Override
    public void setBackFragment(BaseFragment backFragment) {
        this.backFragment = backFragment;
    }

}
