package com.acenosekai.antplay.fragment;

import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import com.acenosekai.antplay.App;
import com.acenosekai.antplay.R;
import com.acenosekai.antplay.sugar.BaseEffectFragment;
import com.acenosekai.antplay.sugar.obj.SliderItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Acenosekai on 1/2/2016.
 * Rock On
 */
public class FreeverbFragment extends BaseEffectFragment {
    @Override
    public List<SliderItem> getSliderItemList() {
        List<SliderItem> sliderItemList = new ArrayList<>();
////
        SliderItem dryMix = new SliderItem();
        dryMix.setLabel(getString(R.string.freeverb_freeze_drymix));
        dryMix.setValue(app.getRegistryFloat(App.FREEVERB_DRYMIX));
        dryMix.setMin(0);
        dryMix.setMax(1);
        dryMix.setStep(20);
        dryMix.setOnPositionChangeListener(new SliderItem.OnPositionChangeListener() {
            @Override
            public void onPositionChanged(float value) {
                app.setRegistry(App.FREEVERB_DRYMIX, String.valueOf(value));
            }
        });
        sliderItemList.add(dryMix);

        SliderItem wetMix = new SliderItem();
        wetMix.setLabel(getString(R.string.freeverb_freeze_wetmix));
        wetMix.setValue(app.getRegistryFloat(App.FREEVERB_WETMIX));
        wetMix.setMin(0);
        wetMix.setMax(3);
        wetMix.setStep(60);
        wetMix.setOnPositionChangeListener(new SliderItem.OnPositionChangeListener() {
            @Override
            public void onPositionChanged(float value) {
                app.setRegistry(App.FREEVERB_WETMIX, String.valueOf(value));
            }
        });
        sliderItemList.add(wetMix);

        SliderItem roomSize = new SliderItem();
        roomSize.setLabel(getString(R.string.freeverb_freeze_room_size));
        roomSize.setValue(app.getRegistryFloat(App.FREEVERB_ROOMSIZE));
        roomSize.setMin(0);
        roomSize.setMax(1);
        roomSize.setStep(20);
        roomSize.setOnPositionChangeListener(new SliderItem.OnPositionChangeListener() {
            @Override
            public void onPositionChanged(float value) {
                app.setRegistry(App.FREEVERB_ROOMSIZE, String.valueOf(value));
            }
        });
        sliderItemList.add(roomSize);

        SliderItem damping = new SliderItem();
        damping.setLabel(getString(R.string.freeverb_freeze_damp));
        damping.setValue(app.getRegistryFloat(App.FREEVERB_DAMP));
        damping.setMin(0);
        damping.setMax(1);
        damping.setStep(20);
        damping.setOnPositionChangeListener(new SliderItem.OnPositionChangeListener() {
            @Override
            public void onPositionChanged(float value) {
                app.setRegistry(App.FREEVERB_DAMP, String.valueOf(value));
            }
        });
        sliderItemList.add(damping);

        SliderItem width = new SliderItem();
        width.setLabel(getString(R.string.freeverb_freeze_width));
        width.setValue(app.getRegistryFloat(App.FREEVERB_WIDTH));
        width.setMin(0);
        width.setMax(1);
        width.setStep(20);
        width.setOnPositionChangeListener(new SliderItem.OnPositionChangeListener() {
            @Override
            public void onPositionChanged(float value) {
                app.setRegistry(App.FREEVERB_WIDTH, String.valueOf(value));
            }
        });
        sliderItemList.add(width);
        return sliderItemList;
    }

    @Override
    public int getFragmentTitle() {
        return R.string.fragment_freeverb_title;
    }

    @Override
    public boolean useFragmentSwitch() {
        return true;
    }

    @Override
    public int getFragmentSwitchKey() {
        return App.FREEVERB;
    }

    @Override
    public int getLayout() {
        return R.layout.fragment_freeverb;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View freeverbView = super.onCreateView(inflater, container, savedInstanceState);
        SwitchCompat freeze = (SwitchCompat) freeverbView.findViewById(R.id.freeverb_freeze_switch);
        freeze.setChecked(app.getRegistry(App.FREEVERB_MODE).equals("1"));
        freeze.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    app.setRegistry(App.FREEVERB_MODE, "1");
                } else {
                    app.setRegistry(App.FREEVERB_MODE, "0");
                }
            }
        });

        return freeverbView;

    }

}
