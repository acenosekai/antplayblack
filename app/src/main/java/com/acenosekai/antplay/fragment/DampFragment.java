package com.acenosekai.antplay.fragment;

import com.acenosekai.antplay.App;
import com.acenosekai.antplay.R;
import com.acenosekai.antplay.sugar.BaseEffectFragment;
import com.acenosekai.antplay.sugar.obj.SliderItem;
import com.un4seen.bass.BASS_FX;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Acenosekai on 1/2/2016.
 * Rock On
 */
public class DampFragment extends BaseEffectFragment {
    @Override
    public List<SliderItem> getSliderItemList() {
        List<SliderItem> sliderItemList = new ArrayList<>();
////
        SliderItem target = new SliderItem();
        target.setLabel(getString(R.string.damp_target));
        target.setValue(app.getRegistryFloat(App.DAMP_TARGET));
        target.setMax(1);
        target.setStep(20);
        target.setOnPositionChangeListener(new SliderItem.OnPositionChangeListener() {
            @Override
            public void onPositionChanged(float value) {
                app.setRegistry(App.DAMP_TARGET, String.valueOf(value));
            }
        });
        sliderItemList.add(target);


        SliderItem quiet = new SliderItem();
        quiet.setLabel(getString(R.string.damp_quiet));
        quiet.setValue(app.getRegistryFloat(App.DAMP_QUIET));
        quiet.setMax(1);
        quiet.setStep(20);
        quiet.setOnPositionChangeListener(new SliderItem.OnPositionChangeListener() {
            @Override
            public void onPositionChanged(float value) {
                app.setRegistry(App.DAMP_QUIET, String.valueOf(value));
            }
        });
        sliderItemList.add(quiet);

        SliderItem rate = new SliderItem();
        rate.setLabel(getString(R.string.damp_rate));
        rate.setValue(app.getRegistryFloat(App.DAMP_RATE));
        rate.setMax(1);
        rate.setStep(20);
        rate.setOnPositionChangeListener(new SliderItem.OnPositionChangeListener() {
            @Override
            public void onPositionChanged(float value) {
                app.setRegistry(App.DAMP_RATE, String.valueOf(value));
            }
        });
        sliderItemList.add(rate);

        SliderItem gain = new SliderItem();
        gain.setLabel(getString(R.string.damp_gain));

        gain.setValue((float) BASS_FX.BASS_BFX_Linear2dB(app.getRegistryFloat(App.DAMP_GAIN)));
        gain.setSuffix(getString(R.string.gain_db));
        gain.setMin(-60);
        gain.setMax(10);
        gain.setStep(150);
        gain.setOnPositionChangeListener(new SliderItem.OnPositionChangeListener() {
            @Override
            public void onPositionChanged(float value) {
                app.setRegistry(App.DAMP_RATE, String.valueOf(BASS_FX.BASS_BFX_dB2Linear(value)));
            }
        });
        sliderItemList.add(gain);

        SliderItem delay = new SliderItem();
        delay.setLabel(getString(R.string.damp_delay));
        delay.setValue(app.getRegistryFloat(App.DAMP_DELAY));
        delay.setMax(10);
        delay.setStep(100);
        delay.setOnPositionChangeListener(new SliderItem.OnPositionChangeListener() {
            @Override
            public void onPositionChanged(float value) {
                app.setRegistry(App.DAMP_DELAY, String.valueOf(value));
            }
        });
        sliderItemList.add(delay);

        return sliderItemList;
    }

    @Override
    public int getFragmentTitle() {
        return R.string.fragment_damp_title;
    }

    @Override
    public boolean useFragmentSwitch() {
        return true;
    }

    @Override
    public int getFragmentSwitchKey() {
        return App.DAMP;
    }
}
