package com.acenosekai.antplay.fragment;


import android.view.View;
import android.widget.TextView;

import com.acenosekai.antplay.R;
import com.acenosekai.antplay.realm.MusicFile;
import com.acenosekai.antplay.sugar.BaseFilesFragment;
import com.acenosekai.antplay.sugar.BaseFragment;
import com.acenosekai.antplay.sugar.FilesMenuBuilder;

import net.soroushjavdan.customfontwidgets.CTextView;

import java.util.List;

/**
 * Created by Acenosekai on 12/12/2015.
 * Rock On
 */
public class MusicFragment extends BaseFilesFragment {
    protected BaseFragment backFragment = new NavigationFragment();
    private String title;
    private List<MusicFile> musicFileList;
    private String parentText;

    private Menu menu = new Menu() {
        @Override
        public void build(View v) {
            new FilesMenuBuilder(activity).buildMenuFiles(v, activity.getMultiSelectHolder());
        }
    };

    @Override
    public Menu getMenu() {

        return menu;
    }

    public void setMenu(Menu menu) {
        this.menu = menu;
    }

    public String getParentText() {
        if (parentText == null) {
            parentText = getString(R.string.all_music);
        }
        return parentText;
    }

    public void setParentText(String parentText) {
        this.parentText = parentText;
    }

    public List<MusicFile> getMusicFileList() {
        return musicFileList;
    }

    public void setMusicFileList(List<MusicFile> musicFileList) {
        this.musicFileList = musicFileList;
    }

    @Override
    public String getTitle() {
        if (title == null) {
            title = getString(R.string.fragment_music_title);
        }
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public List<?> getListObject() {
        return getMusicFileList();
    }

    @Override
    public BaseFragment getBackFragment() {
        return backFragment;
    }

    @Override
    public void setBackFragment(BaseFragment backFragment) {
        this.backFragment = backFragment;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((CTextView) filesView.findViewById(R.id.parent_files)).setText(getParentText());
        filesView.findViewById(R.id.parent_files).setSelected(true);
    }
}
