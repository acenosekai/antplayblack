package com.acenosekai.antplay;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.acenosekai.antplay.fragment.MainFragment;
import com.acenosekai.antplay.realm.Album;
import com.acenosekai.antplay.services.PlaybackService;
import com.acenosekai.antplay.sugar.BaseFragment;
import com.acenosekai.antplay.sugar.BassInit;
import com.acenosekai.antplay.sugar.obj.Artist;
import com.acenosekai.antplay.sugar.obj.Folder;

import java.util.ArrayList;
import java.util.List;

public class BaseActivity extends AppCompatActivity {

    boolean doubleBackToExitPressedOnce = false;
    private App app;
    private PlaybackService playbackService;
    private BaseFragment currentFragment;
    private boolean multiSelectMode = false;
    private Handler h = new Handler();
    private Runnable run = new Runnable() {
        @Override
        public void run() {
            boolean play = false;

            long pos = playbackService.getBytesPosition();
            if (playbackService.isPlaying()) {
                play = true;
            }
            BassInit.reset();
            playbackService.streamInit(true);
            playbackService.setBytesPosition(pos);
            if (play) {
                playbackService.playbackPlay(true);
            }
        }
    };

    protected ServiceConnection mServerConn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder binder) {
            playbackService = ((PlaybackService.PlaybackServiceBinder) binder).getService();
            BassInit.reset();

            app.setOnRegistryChange(new App.OnRegistryChange() {
                @Override
                public void onRegistryChange(int key, String val) {
                    switch (key) {
                        case App.CONFIG_SAMPLE_RATE:
                        case App.CONFIG_BUFFER:
                        case App.CONFIG_SRC:
                        case App.CONFIG_FLOATDSP:
                            h.removeCallbacks(run);
                            h.postDelayed(run, 1000);
                            break;

                    }
                }
            });
            forceChangePage(new MainFragment());
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {

        }
    };
    private List<Object> multiSelectHolder = new ArrayList<>();
    private OnMultiSelectModeChange onMultiSelectModeChange;
    private List<Artist> listArtist;
    private List<Album> listAlbums;
    private Folder rootTree;
    private boolean reInit;

    public List<Object> getMultiSelectHolder() {
        return multiSelectHolder;
    }

    public void addMultiSelectHolder(Object o) {
        multiSelectHolder.add(o);
    }

    public boolean isMultiSelectMode() {
        return multiSelectMode;
    }

    public void setMultiSelectMode(boolean multiSelectMode) {
        this.multiSelectHolder = new ArrayList<>();
        this.multiSelectMode = multiSelectMode;
        if (onMultiSelectModeChange != null) {
            onMultiSelectModeChange.onChange(multiSelectMode);
        }
    }

    public void setOnMultiSelectModeChange(OnMultiSelectModeChange onMultiSelectModeChange) {
        this.onMultiSelectModeChange = onMultiSelectModeChange;
    }

    public boolean isReInit() {
        return reInit;
    }

    public void setReInit(boolean reInit) {
        this.reInit = reInit;
    }

    public PlaybackService getPlaybackService() {
        return playbackService;
    }

    public List<Artist> getListArtist() {
        return listArtist;
    }

    public void setListArtist(List<Artist> listArtist) {
        this.listArtist = listArtist;
    }

    public List<Album> getListAlbums() {
        return listAlbums;
    }

    public void setListAlbums(List<Album> listAlbums) {
        this.listAlbums = listAlbums;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.app = (App) getApplication();
        setContentView(R.layout.activity_base);

        if (playbackService == null) {
            bindServices();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbindService(mServerConn);
    }

    public void changePage(BaseFragment fragment) {
        if (!currentFragment.getClass().getName().equals(fragment.getClass().getName())) {
            forceChangePage(fragment);
        }
    }

    public void forceChangePage(BaseFragment fragment) {
        multiSelectMode = false;
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        transaction.replace(R.id.base_container, fragment);
        transaction.commit();
        currentFragment = fragment;
    }

    private void back() {
        if (isMultiSelectMode()) {
            setMultiSelectMode(false);
        } else {
            if (currentFragment.getBackFragment() != null) {
                changePage(currentFragment.getBackFragment());
            }
        }
    }

    public void goBack(View view) {
        back();
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            if (currentFragment.getClass().getName().equals(MainFragment.class.getName())) {
                super.onBackPressed();
                return;
            }
        }
        this.doubleBackToExitPressedOnce = true;
        back();
        if (currentFragment.getClass().getName().equals(MainFragment.class.getName())) {
            Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();
        }
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 500);
    }

    public Folder getRootTree() {
        return rootTree;
    }

    public void setRootTree(Folder rootTree) {
        this.rootTree = rootTree;
    }

    public void bindServices() {
        try {
            unbindService(mServerConn);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Intent playIntent = new Intent(BaseActivity.this, PlaybackService.class);
        bindService(playIntent, mServerConn, BIND_AUTO_CREATE);
        playIntent.setAction(PlaybackService.BIND_PLAYBACK);
        startService(playIntent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        multiSelectMode = false;
    }

    public interface OnMultiSelectModeChange {
        void onChange(boolean multiSelect);
    }

}
