package com.acenosekai.antplay.services;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.acenosekai.antplay.App;
import com.acenosekai.antplay.BaseActivity;
import com.acenosekai.antplay.realm.MusicFile;
import com.acenosekai.antplay.realm.PlayList;
import com.acenosekai.antplay.sugar.Play;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.realm.RealmList;

/**
 * Created by Acenosekai on 11/25/2015.
 * Rock On
 */
public class PlaybackService extends Service {

    //STATIC FLAG GOES HERE
    public static final String BIND_PLAYBACK = "bind_playback";
    public static final String PREV_ACTION = "playback_prev";
    public static final String PLAY_ACTION = "playback_play";
    public static final String NEXT_ACTION = "playback_next";
    public static final String PAUSE_ACTION = "playback_pause";
    public static final String REPEAT_ONE = "repeat_one";
    public static final String NO_REPEAT = "no_repeat";
    public static final String REPEAT_ALL = "repeat_all";
    public static final String SHUFFLE_ON = "shuffle_on";
    public static final String SHUFFLE_OFF = "shuffle_off";
    public static final String CROSSFADE_ON = "crossFade_on";
    public static final String CROSSFADE_OFF = "crossFade_off";
    public static final String APPLY_EFFECT = "apply_effect";
    public static final String GAIN = "gain";
    public static final String REINITSERVICE = "reinit_service";
    public static final String REINITPLAYLIST = "reinit_playlist";
    public static final int SERVICE_ID = 134;
    private static final String TAG = "antPlayback";
    int playArrPos = 0;
    //PRIVATE VARIABLE HERE
    private Play antPlay = null;
    private OnFinishInterface onFinish = null;
    private OnInitInterface onInit = null;
    private OnRepeatChangeInterface onRepeatChange = null;
    private OnShuffleChangeInterface onShuffleChange = null;
    private OnCrossFadeChangeInterface onCrossFadeChange = null;
    private OnInitialize onInitialize = null;
    private RealmList<MusicFile> musicList;
    private MusicFile musicFile;
    private Integer total = 0;
    private Boolean playing = false;
    private Bitmap bitMapCover = null;
    private Runnable seeker;
    private Handler handler = new Handler();
    private int actualCrossTime = 10000;
    private List<Integer> mainList = new ArrayList<>();
    private int posList = 0;
    private App app;
    private long lastBytePos;
    private boolean initialized = false;
    private OnPlayingStatusChange onPlayingStatusChange;
    private Play[] playArr = new Play[2];//= {new Play(app,"aw"),new Play(app,"aw")};

    public void setOnInitialize(OnInitialize onInitialize) {
        this.onInitialize = onInitialize;
    }

    private Integer getPos() {
        Integer newPos = Integer.valueOf(app.getRegistry(App.SONG_POSITION));
        if (newPos >= this.total) {
            setPos(0);
            return 0;
        }
        return newPos;
    }

    private void setPos(Integer pos) {
        app.setRegistry(App.SONG_POSITION, String.valueOf(pos));
    }

    public int getRepeat() {
        return Integer.valueOf(app.getRegistry(App.REPEAT));
    }

    public void setRepeat(int repeat) {
        if (onRepeatChange != null) {
            onRepeatChange.onRepeatChange(repeat);
        }
    }

    public Boolean getShuffle() {
        return !app.getRegistry(App.SHUFFLE).equals("0");
    }

    public void setShuffle(Boolean shuffle) {
        mainList.clear();
        mainList.addAll(generateFixList());
        int curposVal = mainList.get(getPos());
        if (shuffle) {
            Collections.shuffle(mainList);
        }
        setPosList(mainList.indexOf(curposVal));

        if (onShuffleChange != null) {
            onShuffleChange.onShuffleChange(shuffle);
        }
    }

    public Boolean getCrossFade() {
        return !app.getRegistry(App.CROSSFADE).equals("0");
    }

    public void setCrossFade(Boolean crossFade) {
        if (onCrossFadeChange != null) {
            onCrossFadeChange.onCrossFadeChange(crossFade);
        }
    }

    public void setPosList(int posList) {
        setPos(mainList.get(posList));
        this.posList = posList;
    }

    //GETTER HERE
    public MusicFile getMusicFile() {
        return this.musicFile;
    }

    public int getSecondsPosition() {
        return antPlay.getSecondsPosition();
    }

    public void setSecondsPosition(int position) {
        antPlay.setSecondsPosition(position);
    }

    public long getBytesPosition() {
        return antPlay.getBytesPosition();
    }

    public void setBytesPosition(long position) {
        antPlay.setBytesPosition(position);
    }

    public int getSecondsTotal() {
        return antPlay.getSecondsTotal();
    }

    public long getBytesTotal() {
        return antPlay.getBytesTotal();
    }

    public String getPositionTime() {
        return antPlay.getPositionTime();
    }

    public String getTotalTime() {
        return antPlay.getTotalTime();
    }

    public Bitmap getBitMapCover() {
        return bitMapCover;
    }

    public void setBitMapCover(Bitmap bitMapCover) {
        this.bitMapCover = bitMapCover;
    }

    public Boolean isPlaying() {
        return playing;
    }

    //SETTER HERE
    //Interface Setter
    public void setOnRepeatChange(OnRepeatChangeInterface onRepeatChange) {
        this.onRepeatChange = onRepeatChange;
    }

    public void setOnCrossFadeChange(OnCrossFadeChangeInterface onCrossFadeChange) {
        this.onCrossFadeChange = onCrossFadeChange;
    }

    //Ordinary Setter

    public void setOnFinish(OnFinishInterface onFinish) {
        this.onFinish = onFinish;
    }

    public void setOnInit(OnInitInterface onInit) {
        this.onInit = onInit;
    }

    public void setOnShuffleChange(OnShuffleChangeInterface onShuffleChange) {
        this.onShuffleChange = onShuffleChange;
    }

    public OnPlayingStatusChange getOnPlayingStatusChange() {
        return onPlayingStatusChange;
    }

    public void setOnPlayingStatusChange(OnPlayingStatusChange onPlayingStatusChange) {
        this.onPlayingStatusChange = onPlayingStatusChange;
    }

    public void setPlaying(Boolean playing) {
        this.playing = playing;

        if (onPlayingStatusChange != null) {
            onPlayingStatusChange.OnPlayingStatusChange(playing);
        }

    }

    //SERVICE OVERRIDE FUNCTION HERE
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return new PlaybackServiceBinder();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        this.app = (App) getApplication();
    }

    private void buildForeground() {
        Intent notificationIntent = new Intent(this, BaseActivity.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);

        Intent previousIntent = new Intent(this, PlaybackService.class);
        previousIntent.setAction(PlaybackService.PREV_ACTION);
        PendingIntent ppreviousIntent = PendingIntent.getService(this, 0, previousIntent, PendingIntent.FLAG_CANCEL_CURRENT);

        Intent pauseIntent = new Intent(this, PlaybackService.class);
        pauseIntent.setAction(PlaybackService.PAUSE_ACTION);
        PendingIntent ppauseIntent = PendingIntent.getService(this, 0, pauseIntent, PendingIntent.FLAG_CANCEL_CURRENT);

        Intent nextIntent = new Intent(this, PlaybackService.class);
        nextIntent.setAction(PlaybackService.NEXT_ACTION);
        PendingIntent pnextIntent = PendingIntent.getService(this, 0, nextIntent, PendingIntent.FLAG_CANCEL_CURRENT);

        NotificationCompat.Builder buildNotif = new NotificationCompat.Builder(this)
                .setContentTitle(getMusicFile().getTitle())
                .setTicker(getMusicFile().getTitle())
                .setContentText(getMusicFile().getArtist())
                .setSmallIcon(android.R.drawable.ic_media_play)
                .setContentIntent(pendingIntent)
                .setOngoing(true)
                .addAction(android.R.drawable.ic_media_previous, "Previous", ppreviousIntent)
                .addAction(android.R.drawable.ic_media_pause, "Pause", ppauseIntent)
                .addAction(android.R.drawable.ic_media_next, "Next", pnextIntent);

        if (this.getBitMapCover() != null) {
            buildNotif.setLargeIcon(Bitmap.createScaledBitmap(this.getBitMapCover(), 128, 128, false));
        }
        Notification notification = buildNotif.build();
        startForeground(PlaybackService.SERVICE_ID, notification);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null && intent.getAction() != null) {
            if (intent.getAction().equals(PlaybackService.PLAY_ACTION)) {
                //start service and build notification
                Log.i(TAG, "Start forground app and play the music");
                playbackPlay(true);
            } else if (intent.getAction().equals(PlaybackService.PREV_ACTION)) {
                Log.i(TAG, "Clicked Previous");
                playbackPrev();
            } else if (intent.getAction().equals(PlaybackService.NEXT_ACTION)) {
                Log.i(TAG, "Clicked Next");
                playbackNext(true);
            } else if (intent.getAction().equals(PlaybackService.NO_REPEAT)) {
                setRepeat(Repeat.NO_REPEAT);
            } else if (intent.getAction().equals(PlaybackService.REPEAT_ALL)) {
                setRepeat(Repeat.REPEAT_ALL);
            } else if (intent.getAction().equals(PlaybackService.REPEAT_ONE)) {
                setRepeat(Repeat.REPEAT_ONE);
            } else if (intent.getAction().equals(PlaybackService.SHUFFLE_OFF)) {
                setShuffle(false);
            } else if (intent.getAction().equals(PlaybackService.SHUFFLE_ON)) {
                setShuffle(true);
            } else if (intent.getAction().equals(PlaybackService.CROSSFADE_OFF)) {
                setCrossFade(false);
            } else if (intent.getAction().equals(PlaybackService.CROSSFADE_ON)) {
                setCrossFade(true);
            } else if (intent.getAction().equals(PlaybackService.PAUSE_ACTION)) {
                Log.i(TAG, "Received Stop Foreground Intent");
                playbackPause();

            } else if (intent.getAction().equals(PlaybackService.BIND_PLAYBACK)) {
                //initialize on bind

            } else if (intent.getAction().equals(PlaybackService.APPLY_EFFECT)) {
                //initialize on bind
                if (isPlaying()) {
                    antPlay.applyEffect();
                }
            } else if (intent.getAction().equals(PlaybackService.GAIN)) {
                antPlay.applyGain();
            } else if (intent.getAction().equals(PlaybackService.REINITSERVICE)) {
                reinitService();
            } else if (intent.getAction().equals(PlaybackService.REINITPLAYLIST)) {
                reinitPlayList();
            }

        }
        return START_STICKY;
    }

    public void queryingMusicList() {
        PlayList p = app.getRealm().where(PlayList.class).equalTo("name", "__defaultPlaylist").findFirst();
        this.musicList = p.getMusicFileList(); //app.getRealm().where(MusicFile.class).equalTo("playListList.name", "__defaultPlaylist").equalTo("invalid", false).findAll();
        this.total = this.musicList.size();
    }

    public void reinitPlayList() {
        queryingMusicList();
        setShuffle(getShuffle());
    }

    public void reinitService() {
        if (antPlay != null) {
            lastBytePos = antPlay.getBytesPosition();
        }
        initProperty(true);
        if (isPlaying()) {
            antPlay.setBytesPosition(lastBytePos);
            playbackPlay(true);
        }
    }

    public void initProperty(Boolean reInit) {
        queryingMusicList();

        if (!musicList.isEmpty()) {
            if (reInit) {
                initialized = false;
            }

            if (!initialized) {
                playbackInit();
            }
            setRepeat(getRepeat());

            setPlaying(this.playing);
            if (onRepeatChange != null) {
                onRepeatChange.onRepeatChange(getRepeat());
            }
            if (onShuffleChange != null) {
                onShuffleChange.onShuffleChange(getShuffle());
            }
            if (onCrossFadeChange != null) {
                onCrossFadeChange.onCrossFadeChange(getCrossFade());
            }

            if (onInitialize != null) {
                onInitialize.onSuccess();
            }
        } else {
            if (onInitialize != null) {
                onInitialize.onFailed();
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        antPlay.destroy();
    }

    //PLAYBACK SERVICE FUNCTION HERE
    public void playbackInit() {
        seeker = new Runnable() {
            public void run() {
                if (playing) {
                    setPlaying(true);
                    if (getCrossFade()) {
                        int crossTime = Integer.valueOf(app.getRegistry(App.CROSSTIME));

                        if (getSecondsPosition() >= getSecondsTotal() - (crossTime / 1000)) {
                            actualCrossTime = (getSecondsTotal() - getSecondsPosition()) * 1000;
                            antPlay.fadeOut(actualCrossTime);
//                            BASS.BASS_ChannelSlideAttribute(getActiveChannel(), BASS.BASS_ATTRIB_VOL, 0, actualCrossTime);
                            if (onFinish != null) {
                                onFinish.onFinish();
                            }
                            Log.i(TAG, "finished");
                            playbackNext(false);
                        }
                    } else {
                        if (getPositionTime().equals(getTotalTime())) {
                            if (onFinish != null) {
                                onFinish.onFinish();
                            }
                            Log.i(TAG, "finished");
                            playbackNext(false);
                        }
                    }
                    handler.postDelayed(this, 500);
                }
            }

        };
        setShuffle(getShuffle());
        streamInit(true);
    }

    private List<Integer> generateFixList() {
        List<Integer> fixList = new ArrayList<>();
        //buildFixOrder
        for (int i = 0; i < this.total; i++) {
            fixList.add(i);
        }
        return fixList;
    }

    private void buildPlayArr() {
        playArr[0] = new Play(app);
        playArr[1] = new Play(app);
    }

    public void streamInit(Boolean force) {

        Log.i(TAG, "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        bitMapCover = null;
        this.musicFile = musicList.get(mainList.get(posList));
        if (!new File(musicFile.getPath()).exists()) {
            app.getRealm().beginTransaction();
            musicFile.setInvalid(true);
            app.getRealm().commitTransaction();
            playbackNext(force);
            return;
        }
        if (!musicFile.getInitialized()) {
            app.getRealm().beginTransaction();
            initializeMusicFile(musicFile);
            app.getRealm().commitTransaction();
        }
        if (musicFile.getArtist() != null && musicFile.getAlbum() != null) {
            byte[] art = musicFile.getAlbum().getCoverArt();
            if (art != null && art.length > 0) {
                bitMapCover = BitmapFactory.decodeByteArray(art, 0, art.length);
            }

        }
//        resAntplay = antPlay;
        if (antPlay != null && force) {
            antPlay.stop();
        }

        if (playArr[0] == null) {
            buildPlayArr();
            playArrPos = 0;

        }

        antPlay = playArr[playArrPos];
        antPlay.setFilePath(musicFile.getPath());

        if (playArrPos == 0) {
            playArrPos = 1;
        } else {
            playArrPos = 0;
        }
//        antPlay = new Play(app, musicFile.getPath());
        initialized = true;
        if (onInit != null) {
            onInit.onInit();
        }
    }

    public Boolean playbackNext(Boolean force) {
        int rPos = posList + 1;
        if (posList + 1 >= this.total) {
            switch (getRepeat()) {
                case Repeat.NO_REPEAT:
                    if (force) {
                        rPos = 0;
                    } else {
                        if (playing) {
                            playbackPause();
                        }
                        setPlaying(false);
                        return false;
                    }
                    break;
                case Repeat.REPEAT_ALL:
                case Repeat.REPEAT_ONE:
                    rPos = 0;
                    break;
            }
        }
        if (getRepeat() == Repeat.REPEAT_ONE) {
            if (!force) {
                rPos = posList;
            }
        }
        setPosList(rPos);
        streamInit(force);
        if (playing) {
            playbackPlay(force);
        }
        return true;
    }

    public Boolean playbackPrev() {
        int rPos = posList - 1;
        if (posList - 1 < 0) {
            rPos = total - 1;
        }

        if (playing) {
            if (getSecondsPosition() > 3) {
                setSecondsPosition(0);
            } else {
                setPosList(rPos);
                streamInit(true);
                playbackPlay(true);
            }
        } else {
            setPosList(rPos);
            streamInit(true);
        }

        return true;
    }

//    private boolean mustReinit=false;
//
//    public void setMustReinit(boolean mustReinit) {
//        this.mustReinit = mustReinit;
//    }

//    public void playbackReInitPlay(Boolean force,long pos){
//        streamInit(force);
//        setBytesPosition(pos);
//        playbackPlay(force);
//    }

    //    public void playbackPlay(Boolean force) {
//        playbackPlay(force,0);
//    }
    public void playbackPlay(Boolean force) {

//        if(mustReinit){
//            streamInit(force);
//            mustReinit=false;
//        }

//        setBytesPosition(pos);

        if (!force && getCrossFade()) {
            antPlay.mute();
        }
        buildForeground();
        antPlay.applyEffect();
        antPlay.play();
        if (!force && getCrossFade()) {
            antPlay.fadeIn(actualCrossTime);
        }
        setPlaying(true);

        Log.i(TAG, "Playback play");
        handler.postDelayed(seeker, 500);
    }

    public long playbackPause() {
        long pos = 0;
        if (antPlay != null) {
            pos = antPlay.pause();
            setPlaying(false);
            stopForeground(true);
//            stopSelf();
        }

        Log.i(TAG, "Playback pause");
        return pos;
    }

    public void playbackStop() {
        antPlay.stop();
        setPlaying(false);
        Log.i(TAG, "Playback stop");
    }

    private String secondsToString(Integer sec) {
        int h = 0;
        int m = 0;
        if (sec >= 3600) {
            h = (sec - (sec % 3600)) / 3600;
        }
        int hDev = sec - (h * 3600);
        if (hDev >= 60) {
            m = (hDev - (hDev % 60)) / 60;
        }
        int s = hDev - (m * 60);
        return repairDigit(h) + ":" + repairDigit(m) + ":" + repairDigit(s);

    }

    private String repairDigit(Integer dg) {
        if (dg < 10) {
            return "0" + dg;
        }
        return dg.toString();
    }

    private void initializeMusicFile(MusicFile mf) {
        File fch = new File(mf.getPath());

        if (fch.getPath().endsWith(".mp4")
                || fch.getPath().endsWith(".m4a")
                || fch.getPath().endsWith(".aac")
                //flac format
                || fch.getPath().endsWith(".flac")
                //mp3 format
                || fch.getPath().endsWith(".mp3")
                //vorbis format
                || fch.getPath().endsWith(".ogg")
                //PCM wave format
                || fch.getPath().endsWith(".wav")) {
            try {
                MediaMetadataRetriever metaRetriver = new MediaMetadataRetriever();
                metaRetriver.setDataSource(fch.getPath());
                mf.setTitle(metaRetriver.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE));
                mf.setArtist(metaRetriver.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST));
                mf.setGenre(metaRetriver.extractMetadata(MediaMetadataRetriever.METADATA_KEY_GENRE));
//                mf.setCoverArt(metaRetriver.getEmbeddedPicture());
                mf.setInitialized(true);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }

    public RealmList<MusicFile> getMusicList() {
        return musicList;
    }

    public int getActualPos() {
        return mainList.get(posList);
    }

    public void playbackTo(int position) {
        setPosList(mainList.indexOf(position));
        streamInit(true);
        playbackPlay(true);
        Log.i(TAG, "position : " + position);
        Log.i(TAG, "position list : " + mainList.indexOf(position));
    }

    public interface OnPlayingStatusChange {
        void OnPlayingStatusChange(boolean isPlaying);
    }

    //All Interface here
//    public interface OnPlayingInterface {
//        void onPlaying();
//    }


    public interface OnInitInterface {
        void onInit();
    }

    public interface OnFinishInterface {
        void onFinish();
    }

    public interface OnRepeatChangeInterface {
        void onRepeatChange(int repeat);
    }

    public interface OnShuffleChangeInterface {
        void onShuffleChange(Boolean shuffle);
    }

    public interface OnCrossFadeChangeInterface {
        void onCrossFadeChange(Boolean crossFade);
    }

    public interface OnInitialize {
        void onSuccess();

        void onFailed();
    }

    public class Repeat {
        public static final int NO_REPEAT = 0;
        public static final int REPEAT_ONE = 1;
        public static final int REPEAT_ALL = 2;
    }

    public class PlaybackServiceBinder extends Binder {
        public PlaybackService getService() {
            return PlaybackService.this;
        }
    }
}