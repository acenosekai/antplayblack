package com.acenosekai.antplay.objView;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.SeekBar;

import com.acenosekai.antplay.R;

import net.soroushjavdan.customfontwidgets.CTextView;

/**
 * Created by Acenosekai on 1/1/2016.
 * Rock On
 */
public class SliderHolder extends RecyclerView.ViewHolder {
    private View itemView;
    private CTextView label;
    private CTextView value;
    private SeekBar slider;

    public SliderHolder(View itemView) {
        super(itemView);
        this.itemView = itemView;
        this.label = (CTextView) itemView.findViewById(R.id.slider_label);
        this.value = (CTextView) itemView.findViewById(R.id.slider_value);
        this.slider = (SeekBar) itemView.findViewById(R.id.slider);
    }

    public View getItemView() {
        return itemView;
    }

    public CTextView getLabel() {
        return label;
    }

    public CTextView getValue() {
        return value;
    }

    public SeekBar getSlider() {
        return slider;
    }
}
