package com.acenosekai.antplay.objView;

import com.acenosekai.antplay.R;

/**
 * Created by Acenosekai on 12/6/2015.
 * Rock On
 */
public class DividerNavigation extends Navigation {

    @Override
    public int getLayoutId() {
        return R.layout.navigation_divider;
    }
}
