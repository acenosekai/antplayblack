package com.acenosekai.antplay.objView;

import android.content.Context;
import android.view.View;

import com.acenosekai.antplay.R;

/**
 * Created by Acenosekai on 12/6/2015.
 * Rock On
 */
public class SwitchNavigation extends Navigation {

    @Override
    public int getLayoutId() {
        return R.layout.navigation_switch;
    }

    @Override
    public void onBind(Context context, View v) {
        super.onBind(context, v);

    }
}
