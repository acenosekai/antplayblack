package com.acenosekai.antplay.objView;

import android.view.View;
import android.widget.SeekBar;

import com.acenosekai.antplay.R;
import com.mikepenz.iconics.view.IconicsImageView;

/**
 * Created by Acenosekai on 1/4/2016.
 * Rock On
 */
public class SliderEqHolder extends SliderHolder {

    private IconicsImageView setting;

    public SliderEqHolder(View itemView) {
        super(itemView);
        this.setting = (IconicsImageView) itemView.findViewById(R.id.eq_setting);
    }

    public IconicsImageView getSetting() {
        return setting;
    }
}
