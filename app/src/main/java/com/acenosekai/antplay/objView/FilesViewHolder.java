package com.acenosekai.antplay.objView;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.acenosekai.antplay.R;
import com.mikepenz.iconics.view.IconicsImageView;

import net.soroushjavdan.customfontwidgets.CTextView;

/**
 * Created by Acenosekai on 12/12/2015.
 * Rock On
 */
public class FilesViewHolder extends RecyclerView.ViewHolder {
    private View itemView;
    private CTextView name;
    private CTextView description;
    private IconicsImageView listIcon;
    private ImageView coverAlbum;

    public FilesViewHolder(View itemView) {
        super(itemView);
        this.itemView = itemView;
        this.name = (CTextView) itemView.findViewById(R.id.directory_name);
        this.listIcon = (IconicsImageView) itemView.findViewById(R.id.list_icon);
        this.coverAlbum = (ImageView) itemView.findViewById(R.id.album_cover);
        this.description = (CTextView) itemView.findViewById(R.id.directory_desc);
    }

    public View getItemView() {
        return itemView;
    }

    public CTextView getName() {
        return name;
    }

    public IconicsImageView getListIcon() {
        return listIcon;
    }

    public ImageView getCoverAlbum() {
        return coverAlbum;
    }

    public CTextView getDescription() {
        return description;
    }
}
