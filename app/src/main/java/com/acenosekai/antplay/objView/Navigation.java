package com.acenosekai.antplay.objView;

import android.content.Context;
import android.view.View;

import com.acenosekai.antplay.R;

/**
 * Created by Acenosekai on 12/6/2015.
 * Rock On
 */
public class Navigation {
    protected String name;
    private View.OnClickListener onClickListener;

    public int getLayoutId() {
        return R.layout.navigation_link;
    }

    public String getName() {
        return name;
    }

    public Navigation setName(String name) {
        this.name = name;
        return this;
    }

    public Navigation setOnClickListener(View.OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
        return this;
    }

    public void onBind(Context context, View v) {
        if (this.onClickListener != null) {
            v.setOnClickListener(this.onClickListener);
        }
    }


}
