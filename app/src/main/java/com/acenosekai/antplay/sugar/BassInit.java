package com.acenosekai.antplay.sugar;


import android.util.Log;

import com.acenosekai.antplay.App;
import com.un4seen.bass.BASS;
import com.un4seen.bass.BASS_FX;

import java.io.File;

/**
 * Created by Acenosekai on 11/25/2015.
 * Rock On
 */
public class BassInit {
    private static final String TAG = "BassInit";
    private static BassInit instance;

    private BassInit() {

    }

    public static void destroy() {
        BASS.BASS_Free();
        instance = null;
    }

    public static void reset() {
        instance = null;
    }

    public static void configure(App app) {
        BASS.BASS_SetConfig(BASS.BASS_CONFIG_FLOATDSP, (int) app.getRegistryFloat(App.CONFIG_FLOATDSP));
        BASS.BASS_SetConfig(BASS.BASS_CONFIG_BUFFER, (int) app.getRegistryFloat(App.CONFIG_BUFFER));
        BASS.BASS_SetConfig(BASS.BASS_CONFIG_DEV_BUFFER, (int) app.getRegistryFloat(App.CONFIG_BUFFER));
        BASS.BASS_SetConfig(BASS.BASS_CONFIG_SRC, (int) app.getRegistryFloat(App.CONFIG_SRC));
        BASS.BASS_SetConfig(BASS.BASS_CONFIG_SRC_SAMPLE, (int) app.getRegistryFloat(App.CONFIG_SRC));
    }

    public static synchronized BassInit getInstance(App app) {
        if (instance == null) {
            BASS.BASS_Free();
            instance = new BassInit();
            if (!BASS.BASS_Init(-1, (int) app.getRegistryFloat(App.CONFIG_SAMPLE_RATE), BASS.BASS_DEVICE_LATENCY)) {
                Log.i(TAG, "Can't initialize device");
            }
            BASS.BASS_INFO info = new BASS.BASS_INFO();
            if (BASS.BASS_GetInfo(info)) {
                Log.i(TAG, "Min Buffer :" + info.minbuf);
                Log.i(TAG, "Direct Sound Ver :" + info.dsver);
                Log.i(TAG, "Latency :" + info.latency);
                Log.i(TAG, "speakers :" + info.speakers);
                Log.i(TAG, "freq :" + info.freq);
            }
            configure(app);
            BASS_FX.BASS_FX_GetVersion();
            String[] list = new File(app.getApplicationInfo().nativeLibraryDir).list();
            for (String s : list) {
                BASS.BASS_PluginLoad(app.getApplicationInfo().nativeLibraryDir + "/" + s, 0);
            }
        }

        return instance;
    }
}
