package com.acenosekai.antplay.sugar.obj;

/**
 * Created by Acenosekai on 12/14/2015.
 * Rock On
 */
public class Artist {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
