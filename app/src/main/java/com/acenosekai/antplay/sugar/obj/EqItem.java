package com.acenosekai.antplay.sugar.obj;

public class EqItem {
    private float gain;
    private float bandwidth;
    private float frequency;

    public EqItem(float gain, float bandwidth, float frequency) {
        this.gain = gain;
        this.bandwidth = bandwidth;
        this.frequency = frequency;
    }

    public EqItem(int gain, int bandwidth, int frequency) {
        this.gain = (float) gain;
        this.bandwidth = (float) bandwidth;
        this.frequency = (float) frequency;
    }

    public float getGain() {
        return gain;
    }

    public void setGain(float gain) {
        this.gain = gain;
    }

    public float getBandwidth() {
        return bandwidth;
    }

    public void setBandwidth(float bandwidth) {
        this.bandwidth = bandwidth;
    }

    public float getFrequency() {
        return frequency;
    }

    public void setFrequency(float frequency) {
        this.frequency = frequency;
    }
}
