package com.acenosekai.antplay.sugar.obj;

/**
 * Created by Acenosekai on 1/1/2016.
 * Rock On
 */
public class SliderItem {
    private String label;
    private String suffix;
    private float value = 0;
    private float min = 0;
    private float max = 100;
    private int step = 100;
    private OnPositionChangeListener onPositionChangeListener;

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    public float getMin() {
        return min;
    }

    public void setMin(float min) {
        this.min = min;
    }

    public float getMax() {
        return max;
    }

    public void setMax(float max) {
        this.max = max;
    }

    public OnPositionChangeListener getOnPositionChangeListener() {
        return onPositionChangeListener;
    }

    public void setOnPositionChangeListener(OnPositionChangeListener onPositionChangeListener) {
        this.onPositionChangeListener = onPositionChangeListener;
    }

    public int getStep() {
        return step;
    }

    public void setStep(int step) {
        this.step = step;
    }

    public interface OnPositionChangeListener {
        void onPositionChanged(float value);
    }
}
