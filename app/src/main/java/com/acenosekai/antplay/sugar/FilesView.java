package com.acenosekai.antplay.sugar;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.view.View;

import com.acenosekai.antplay.App;
import com.acenosekai.antplay.BaseActivity;
import com.acenosekai.antplay.R;
import com.acenosekai.antplay.fragment.FolderFragment;
import com.acenosekai.antplay.fragment.MusicFragment;
import com.acenosekai.antplay.objView.FilesViewHolder;
import com.acenosekai.antplay.realm.Album;
import com.acenosekai.antplay.realm.MusicFile;
import com.acenosekai.antplay.realm.PlayList;
import com.acenosekai.antplay.sugar.obj.Artist;
import com.acenosekai.antplay.sugar.obj.Folder;
import com.mikepenz.community_material_typeface_library.CommunityMaterial;

import io.realm.RealmResults;

/**
 * Created by Acenosekai on 12/12/2015.
 * Rock On
 */
public class FilesView {
    private static final int MUSIC = 0;
    private static final int ARTIST = 1;
    private static final int ALBUM = 2;
    private static final int FOLDER = 3;
    private static final int PLAYLIST = 4;
    private BaseActivity activity;
    private App app;
    private FilesViewHolder holder;

    public FilesView(BaseActivity activity, FilesViewHolder holder) {
        this.activity = activity;
        this.app = (App) activity.getApplication();
        this.holder = holder;
    }

    public void playlistView(final PlayList objectItem, final BaseFragment backFragment) {
        holder.getName().setText(objectItem.getName());
        holder.getDescription().setText(objectItem.getMusicFileList().size() + " song(s)");
        iconView(PLAYLIST);

        multiSelectHandler(objectItem, new OnClickNonMultiSelect() {
            @Override
            public void onClick() {
                MusicFragment mf = new MusicFragment();
                mf.setMusicFileList(objectItem.getMusicFileList());
                mf.setBackFragment(backFragment);
                mf.setTitle(app.getString(R.string.fragment_play_list_title));
                mf.setParentText(objectItem.getName());
                mf.setMenu(new BaseFilesFragment.Menu() {
                    @Override
                    public void build(View v) {
                        new FilesMenuBuilder(activity).buildMenuMusicPlaylist(v, activity.getMultiSelectHolder(), objectItem);
                    }
                });
                activity.changePage(mf);
            }
        });


    }

    public void folderView(final Folder objectItem, final FolderFragment folderFragment) {

        holder.getName().setText(objectItem.getName());


        if (objectItem.isDirectory()) {
            iconView(FOLDER);
            holder.getDescription().setVisibility(View.INVISIBLE);
        } else {
            MusicFile musicFile = app.getRealm().where(MusicFile.class).equalTo("path", objectItem.getPath()).findFirst();
            iconView(MUSIC);
            holder.getDescription().setText(musicFile.getArtist() + " - " + musicFile.getAlbum().getName());

            holder.getDescription().setVisibility(View.VISIBLE);
        }


        multiSelectHandler(objectItem, new OnClickNonMultiSelect() {
            @Override
            public void onClick() {
                if (objectItem.isDirectory()) {
                    folderFragment.generateAdapter(objectItem.getChilds());
                    folderFragment.setCurrentFolder(objectItem);
                }
            }
        });
    }

    public void albumView(final Album objectItem, final BaseFragment backFragment) {
        final RealmResults<MusicFile> musicFileRealmResults = app.getRealm().where(MusicFile.class).equalTo("album.key", objectItem.getKey()).findAll();
        holder.getName().setText(objectItem.getName());
        holder.getDescription().setText(objectItem.getArtist() + " - " + musicFileRealmResults.size() + " song(s)");
        holder.getCoverAlbum().setTag(objectItem);

        final byte[] cover = objectItem.getCoverArt();
        if (objectItem.getCoverArt() != null && objectItem.getCoverArt().length > 0) {
            Handler h = new Handler();
            h.postDelayed(new Runnable() {
                @Override
                public void run() {
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            final Bitmap bitMapCover = BitmapFactory.decodeByteArray(cover, 0, cover.length);
                            activity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    holder.getCoverAlbum().setImageBitmap(bitMapCover);
                                    holder.getCoverAlbum().setVisibility(View.VISIBLE);
                                }
                            });
                        }
                    }).start();
                }
            }, 200);
        }

        iconView(ALBUM);

        multiSelectHandler(objectItem, new OnClickNonMultiSelect() {
            @Override
            public void onClick() {

                MusicFragment mf = new MusicFragment();
                mf.setMusicFileList(musicFileRealmResults);
                mf.setBackFragment(backFragment);
                mf.setTitle(app.getString(R.string.fragment_album_title));
                mf.setParentText(objectItem.getName());
                activity.changePage(mf);
            }
        });
    }

    public void artistView(final Artist objectItem, final BaseFragment backFragment) {
        final RealmResults<MusicFile> musicFileRealmResults = app.getRealm().where(MusicFile.class).equalTo("artist", objectItem.getName()).findAll();
        holder.getName().setText(objectItem.getName());
        holder.getDescription().setText(musicFileRealmResults.size() + " song(s)");
        iconView(ARTIST);

        multiSelectHandler(objectItem, new OnClickNonMultiSelect() {
            @Override
            public void onClick() {

                MusicFragment mf = new MusicFragment();
                mf.setMusicFileList(musicFileRealmResults);
                mf.setBackFragment(backFragment);
                mf.setTitle(app.getString(R.string.fragment_artist_title));
                mf.setParentText(objectItem.getName());
                activity.changePage(mf);
            }
        });
    }

    public void musicView(final MusicFile objectItem) {
        holder.getName().setText(objectItem.getTitle());
        holder.getDescription().setText(objectItem.getArtist() + " - " + objectItem.getAlbum().getName());
        iconView(MUSIC);

        multiSelectHandler(objectItem, new OnClickNonMultiSelect() {
            @Override
            public void onClick() {
                //do nothing
            }
        });
    }

    private void multiSelectHandler(final Object objectItem, final OnClickNonMultiSelect onClickNonMultiselect) {
        holder.getItemView().setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (!activity.isMultiSelectMode()) {
                    activity.setMultiSelectMode(true);
                    holder.getItemView().setBackgroundColor(ContextCompat.getColor(activity, R.color.colorSemiTransparent));
                    activity.addMultiSelectHolder(objectItem);
                }
                return true;
            }
        });
        holder.getItemView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (activity.isMultiSelectMode()) {
                    if (activity.getMultiSelectHolder().contains(objectItem)) {
                        //remove
                        activity.getMultiSelectHolder().remove(objectItem);
                        holder.getItemView().setBackgroundColor(ContextCompat.getColor(activity, R.color.colorBlack));
                    } else {
                        activity.addMultiSelectHolder(objectItem);
                        holder.getItemView().setBackgroundColor(ContextCompat.getColor(activity, R.color.colorSemiTransparent));
                    }
                } else {
                    onClickNonMultiselect.onClick();
                }
            }
        });

        if (activity.isMultiSelectMode() && activity.getMultiSelectHolder().contains(objectItem)) {
            holder.getItemView().setBackgroundColor(ContextCompat.getColor(activity, R.color.colorSemiTransparent));
        } else {
            holder.getItemView().setBackgroundColor(ContextCompat.getColor(activity, R.color.colorBlack));
        }
    }

    private void iconView(int type) {
        holder.getCoverAlbum().setImageDrawable(null);
        holder.getCoverAlbum().setVisibility(View.INVISIBLE);
        switch (type) {
            case MUSIC:
                holder.getListIcon().setIcon(CommunityMaterial.Icon.cmd_music_circle);
                break;
            case ARTIST:
                holder.getListIcon().setIcon(CommunityMaterial.Icon.cmd_music_box_outline);
                break;
            case ALBUM:
                holder.getListIcon().setIcon(CommunityMaterial.Icon.cmd_folder_multiple_outline);
                break;
            case FOLDER:
                holder.getListIcon().setIcon(CommunityMaterial.Icon.cmd_folder_outline);
                break;
            case PLAYLIST:
                holder.getListIcon().setIcon(CommunityMaterial.Icon.cmd_library_music);
                break;
        }
    }

    public interface OnClickNonMultiSelect {
        void onClick();
    }

}
