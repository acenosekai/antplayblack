package com.acenosekai.antplay.sugar;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.acenosekai.antplay.App;
import com.acenosekai.antplay.BaseActivity;
import com.acenosekai.antplay.R;

/**
 * Created by Acenosekai on 12/12/2015.
 * Rock On
 */
public class BaseFragment extends Fragment {
    protected BaseFragment backFragment;
    protected BaseActivity activity;
    protected App app;

    public BaseFragment getBackFragment() {
        return backFragment;
    }

    public void setBackFragment(BaseFragment backFragment) {
        this.backFragment = backFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.activity = (BaseActivity) getActivity();
        this.app = (App) activity.getApplication();
        return inflater.inflate(R.layout.fragment_files, container, false);
    }
}
