package com.acenosekai.antplay.sugar;

import android.graphics.Point;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;

import com.acenosekai.antplay.App;
import com.acenosekai.antplay.BaseActivity;
import com.acenosekai.antplay.R;
import com.acenosekai.antplay.adapter.PlayListDialogAdapter;
import com.acenosekai.antplay.fragment.MainFragment;
import com.acenosekai.antplay.fragment.NavigationFragment;
import com.acenosekai.antplay.fragment.PlayListBaseFragment;
import com.acenosekai.antplay.realm.Album;
import com.acenosekai.antplay.realm.MusicFile;
import com.acenosekai.antplay.realm.PlayList;
import com.acenosekai.antplay.sugar.obj.Artist;
import com.acenosekai.antplay.sugar.obj.Folder;
import com.rey.material.app.Dialog;
import com.rey.material.widget.EditText;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmList;
import io.realm.RealmQuery;
import io.realm.RealmResults;

/**
 * Created by Acenosekai on 12/10/2015.
 * Rock On
 */
public class FilesMenuBuilder {
    private BaseActivity activity;
    private App app;
    private PlayList defaultPlaylist;

    public FilesMenuBuilder(BaseActivity activity) {
        this.activity = activity;
        this.app = (App) activity.getApplication();
        this.defaultPlaylist = app.getRealm().where(PlayList.class).equalTo("name", "__defaultPlaylist").findFirst();
    }

    public void playHandler(List<MusicFile> musicList) {
        activity.getPlaybackService().playbackPause();
        app.getRealm().beginTransaction();
        defaultPlaylist.getMusicFileList().clear();
        defaultPlaylist.getMusicFileList().addAll(musicList);
        app.getRealm().copyToRealmOrUpdate(defaultPlaylist);
        app.getRealm().commitTransaction();
        activity.setReInit(true);
        activity.changePage(new MainFragment());
    }

    public void toCurrentPlaylist(List<MusicFile> musicList) {
        app.getRealm().beginTransaction();
        defaultPlaylist.getMusicFileList().addAll(musicList);
        app.getRealm().copyToRealmOrUpdate(defaultPlaylist);
        app.getRealm().commitTransaction();
        activity.getPlaybackService().reinitPlayList();
        activity.setMultiSelectMode(false);
    }

    public void toPlaylist(List<MusicFile> musicList) {
        toPlaylist(musicList, null);
    }

    public void toPlaylist(List<MusicFile> musicList, String exceptionPlaylist) {
        Point size = new Point();
        WindowManager w = activity.getWindowManager();
        w.getDefaultDisplay().getSize(size);
        RealmQuery<PlayList> q = app.getRealm().where(PlayList.class).notEqualTo("name", "__defaultPlaylist");
        if (exceptionPlaylist != null) {
            q.notEqualTo("name", exceptionPlaylist);
        }

        RealmResults<PlayList> playLists = q.findAll();
        View dialogPlaylistView = activity.getLayoutInflater().inflate(R.layout.dialog_list_basic, null);
        RecyclerView recyclerView = (RecyclerView) dialogPlaylistView.findViewById(R.id.string_list);
        LinearLayoutManager llm = new LinearLayoutManager(activity);
        recyclerView.setLayoutManager(llm);
        recyclerView.setHasFixedSize(true);
        final Dialog toPlaylistDialog = new Dialog(activity);
        PlayListDialogAdapter adapter = new PlayListDialogAdapter(activity, playLists, musicList, toPlaylistDialog);
        recyclerView.setAdapter(adapter);
        toPlaylistDialog.applyStyle(R.style.Material_App_Dialog)
                .negativeAction(R.string.ant_dialog_cancel)
                .title(R.string.dialog_to_play_list_title)
                .contentView(dialogPlaylistView)
                .negativeActionClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        toPlaylistDialog.hide();
                    }
                })
                .cancelable(true)
                .canceledOnTouchOutside(true)
                .layoutParams(((int) (size.x * 0.8)), ((int) (size.y * 0.8))).show();
    }

    private void toNewPlaylist(final List<MusicFile> musicList) {
        View dialogView = activity.getLayoutInflater().inflate(R.layout.dialog_new_play_list, null);
        final EditText et = (EditText) dialogView.findViewById(R.id.play_list_name);
        final Dialog mDialog = new Dialog(activity);
        et.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    toNewPlaylistPositive(et, musicList, mDialog);
                    return true;
                }
                return false;
            }
        });
        Point size = new Point();
        WindowManager w = activity.getWindowManager();
        w.getDefaultDisplay().getSize(size);

        mDialog.applyStyle(R.style.Material_App_Dialog)
                .title(R.string.dialog_new_play_list_title)
                .positiveAction(R.string.ant_dialog_ok)
                .negativeAction(R.string.ant_dialog_cancel)
                .contentView(dialogView)
                .canceledOnTouchOutside(true)
                .positiveActionClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        toNewPlaylistPositive(et, musicList, mDialog);
                    }
                })
                .negativeActionClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mDialog.hide();
                    }
                })
                .cancelable(true).layoutParams(((int) (size.x * 0.8)), 400)
                .canceledOnTouchOutside(true)
                .show();
    }

    private void toNewPlaylistPositive(EditText et, List<MusicFile> musicList, Dialog mDialog) {
        RealmResults<PlayList> cekPlaylist = app.getRealm().where(PlayList.class).equalTo("name", et.getText().toString()).findAll();
        if (cekPlaylist.isEmpty()) {
            app.getRealm().beginTransaction();
            PlayList p = new PlayList();
            p.setName(et.getText().toString());
            p.setType(1);
            p.setMusicFileList(new RealmList<MusicFile>());
            p.getMusicFileList().addAll(musicList);
            app.getRealm().copyToRealmOrUpdate(p);
            app.getRealm().commitTransaction();
            mDialog.hide();
            activity.setMultiSelectMode(false);
        } else {
            et.setHelper(activity.getString(R.string.dialog_new_play_list_error));
        }
    }


    private void deletePlaylist(PlayList p) {
        app.getRealm().beginTransaction();
        p.removeFromRealm();
        app.getRealm().commitTransaction();
    }

    public void buildMenuMusicPlaylist(View v, final List<Object> multiSelectHolder, final PlayList p) {
        final List<MusicFile> musicList = holderToMusicFile(multiSelectHolder);

        PopupMenu popup = new PopupMenu(activity, v);
        popup.getMenuInflater().inflate(R.menu.menu_playlist, popup.getMenu());
        popup.show();

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.play:
                        playHandler(musicList);
                        break;
                    case R.id.toCurrentPlay:
                        toCurrentPlaylist(musicList);
                        break;
                    case R.id.delete:
//                        for (Object o : multiSelectHolder) {
                        boolean deleted = false;
                        app.getRealm().beginTransaction();
                        p.getMusicFileList().removeAll(musicList);
                        if (p.getMusicFileList().isEmpty()) {
                            p.removeFromRealm();
                            deleted = true;
                        } else {
                            app.getRealm().copyToRealmOrUpdate(p);
                        }
                        app.getRealm().commitTransaction();

                        if (deleted) {
                            if (app.getRealm().where(PlayList.class).notEqualTo("name", "__defaultPlaylist").findAll().isEmpty()) {
                                activity.changePage(new NavigationFragment());
                            } else {
                                activity.changePage(new PlayListBaseFragment());
                            }
                        } else {
                            activity.setMultiSelectMode(false);
                        }

                        break;
                }
                return true;
            }
        });
    }

    public void buildMenuPlaylist(View v, final List<Object> multiSelectHolder) {
        final List<MusicFile> musicList = holderToMusicFile(multiSelectHolder);

        PopupMenu popup = new PopupMenu(activity, v);
        popup.getMenuInflater().inflate(R.menu.menu_playlist, popup.getMenu());
        popup.show();

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.play:
                        playHandler(musicList);
                        break;
                    case R.id.toCurrentPlay:
                        toCurrentPlaylist(musicList);
                        break;
                    case R.id.delete:
                        for (Object o : multiSelectHolder) {
                            PlayList p = (PlayList) o;
                            deletePlaylist(p);
                            activity.setMultiSelectMode(false);
                        }
                        break;
                }
                return true;
            }
        });
    }

    public void buildMenuFiles(View v, final List<Object> multiSelectHolder) {
        final List<MusicFile> musicList = holderToMusicFile(multiSelectHolder);
        PopupMenu popup = new PopupMenu(activity, v);
        popup.getMenuInflater().inflate(R.menu.menu_files, popup.getMenu());
        popup.show();
        if (app.getRealm().where(PlayList.class).notEqualTo("name", "__defaultPlaylist").findAll().isEmpty()) {
            popup.getMenu().findItem(R.id.toPlaylist).setVisible(false);
        }
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.play:
                        playHandler(musicList);
                        break;
                    case R.id.toCurrentPlay:
                        toCurrentPlaylist(musicList);
                        break;
                    case R.id.toPlaylist:
                        toPlaylist(musicList);
                        break;
                    case R.id.toNewPlaylist:
                        toNewPlaylist(musicList);
                        break;
                }
                return true;
            }
        });

    }

    public List<MusicFile> holderToMusicFile(List<Object> multiSelectHolder) {
        List<MusicFile> musicFiles = new ArrayList<>();
        for (Object o : multiSelectHolder) {
            try {
                Folder folder = (Folder) o;
                musicFiles.addAll(app.getRealm().where(MusicFile.class).beginsWith("path", folder.getPath()).findAll());
            } catch (Exception e) {
                try {
                    Artist artist = (Artist) o;
                    musicFiles.addAll(app.getRealm().where(MusicFile.class).beginsWith("artist", artist.getName()).findAll());
                } catch (Exception e2) {
                    try {
                        MusicFile musicFile = (MusicFile) o;
                        musicFiles.add(musicFile);

                    } catch (Exception e3) {
                        try {
                            Album album = (Album) o;
                            musicFiles.addAll(app.getRealm().where(MusicFile.class).beginsWith("album.key", album.getKey()).findAll());
                        } catch (Exception e4) {
                            try {
                                PlayList playList = (PlayList) o;
                                musicFiles.addAll(playList.getMusicFileList());
                            } catch (Exception e5) {
                                e5.printStackTrace();
                            }
                        }

                    }
                }

            }
        }

        return musicFiles;
    }

}
