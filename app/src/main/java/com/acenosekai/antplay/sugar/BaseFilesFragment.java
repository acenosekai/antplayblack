package com.acenosekai.antplay.sugar;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.acenosekai.antplay.BaseActivity;
import com.acenosekai.antplay.R;
import com.acenosekai.antplay.adapter.FilesBaseAdapter;

import net.soroushjavdan.customfontwidgets.CTextView;

import java.util.List;

/**
 * Created by Acenosekai on 12/14/2015.
 * Rock On
 */


abstract public class BaseFilesFragment extends BaseFragment {
    protected View filesView;
    protected FilesBaseAdapter adapter;

    abstract public String getTitle();

    abstract public List<?> getListObject();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        this.filesView = inflater.inflate(R.layout.fragment_files, container, false);
        ((CTextView) filesView.findViewById(R.id.fragment_title)).setText(getTitle());

        RecyclerView contentList = (RecyclerView) filesView.findViewById(R.id.content_list);
        LinearLayoutManager llm = new LinearLayoutManager(activity);
        contentList.setLayoutManager(llm);
        contentList.setHasFixedSize(true);

        this.adapter = new FilesBaseAdapter(activity, getListObject());
        contentList.setAdapter(adapter);

        activity.setOnMultiSelectModeChange(new BaseActivity.OnMultiSelectModeChange() {
            @Override
            public void onChange(boolean multiSelect) {
                if (multiSelect) {
                    filesView.findViewById(R.id.nav_bar_multi).setVisibility(View.VISIBLE);
                } else {
                    filesView.findViewById(R.id.nav_bar_multi).setVisibility(View.INVISIBLE);
                    adapter.notifyDataSetChanged();
                }
            }
        });

        filesView.findViewById(R.id.select_all).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.getMultiSelectHolder().clear();
                activity.getMultiSelectHolder().addAll(getListObject());
                adapter.notifyDataSetChanged();
            }
        });

        filesView.findViewById(R.id.un_select_all).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.getMultiSelectHolder().clear();
                adapter.notifyDataSetChanged();
            }
        });

        filesView.findViewById(R.id.menu_multi_select).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getMenu().build(v);
            }
        });

        filesView.findViewById(R.id.play_all).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FilesMenuBuilder fmb = new FilesMenuBuilder(activity);
                fmb.playHandler(fmb.holderToMusicFile((List<Object>) getListObject()));
            }
        });

        return filesView;
    }

    public Menu getMenu() {
        return new Menu() {
            @Override
            public void build(View v) {
                new FilesMenuBuilder(activity).buildMenuFiles(v, activity.getMultiSelectHolder());
            }
        };
    }


    public interface Menu {
        void build(View v);
    }

}
