package com.acenosekai.antplay.sugar;

import android.util.Log;

import com.acenosekai.antplay.App;
import com.acenosekai.antplay.sugar.obj.EqItem;
import com.un4seen.bass.BASS;
import com.un4seen.bass.BASS_FX;

import java.util.HashMap;

/**
 * Created by Acenosekai on 11/25/2015.
 * Rock On
 */
public class Play {

    private static final String TAG = "antPlay";
    int chan = 0;
    private String filePath;
    private App app;
    private float maxVolume;
    private HashMap<Integer, Integer> effectMap = new HashMap<>();

    public Play(App app) {

        this.app = app;
//        this.filePath = filePath;
//        maxVolume = 1;//Float.valueOf(app.getRegistry(App.GAIN));
//        BassInit.getInstance(app);
//        BASS.BASS_StreamFree(chan);
//        chan = 0;
//
//        if (chan == 0) {
//            if ((chan = BASS.BASS_StreamCreateFile(filePath, 0, 0, BASS.BASS_SAMPLE_FLOAT)) == 0) {
//                // whatever it is, it ain't playable
//                Log.i(TAG, "Can't play the file");
//                return;
//            }
//            BASS.BASS_ChannelSetAttribute(chan, BASS.BASS_ATTRIB_SRC, 3);
//            BASS.BASS_ChannelSetAttribute(chan, BASS.BASS_ATTRIB_VOL, maxVolume);
//        }


    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
        maxVolume = 1;//Float.valueOf(app.getRegistry(App.GAIN));
        BassInit.getInstance(app);
        BASS.BASS_StreamFree(chan);
        chan = 0;

        if (chan == 0) {
            if ((chan = BASS.BASS_StreamCreateFile(filePath, 0, 0, BASS.BASS_SAMPLE_FLOAT)) == 0) {
                // whatever it is, it ain't playable
                Log.i(TAG, "Can't play the file");
                return;
            }
            BASS.BASS_ChannelSetAttribute(chan, BASS.BASS_ATTRIB_SRC, 3);
            BASS.BASS_ChannelSetAttribute(chan, BASS.BASS_ATTRIB_VOL, maxVolume);
        }
    }

    public void mute() {
        BASS.BASS_ChannelSetAttribute(chan, BASS.BASS_ATTRIB_VOL, 0);
    }

    public void fadeOut(int second) {
        BASS.BASS_ChannelSlideAttribute(chan, BASS.BASS_ATTRIB_VOL, -1, second);
    }

    public void fadeIn(int second) {
        BASS.BASS_ChannelSlideAttribute(chan, BASS.BASS_ATTRIB_VOL, maxVolume, second);
    }

    public void play() {
        BASS.BASS_ChannelPlay(chan, false);
    }

    public long pause() {
        long pos = getBytesPosition(); //get last position
        BASS.BASS_ChannelPause(chan);
        setBytesPosition(pos);
        return pos;
    }

    public void stop() {
        BASS.BASS_ChannelStop(chan);
        BASS.BASS_StreamFree(chan);
    }

    public void destroy() {
        BassInit.getInstance(app).destroy();
    }

    public void applyEffect() {
        enablePeaeq();
        enableFreeverb();
        enableDamp();
        enableCompressor();
        enableVolume();
//        enableChorus();
    }

    public void applyGain() {
        maxVolume = 1;//Float.valueOf(app.getRegistry(App.GAIN));
        BASS.BASS_ChannelSetAttribute(chan, BASS.BASS_ATTRIB_VOL, maxVolume);
    }

    public void enableDamp() {
        if (effectMap.get(Effect.DAMP) == null) {
            effectMap.put(Effect.DAMP, BASS.BASS_ChannelSetFX(chan, BASS_FX.BASS_FX_BFX_DAMP, 0));
        }

        BASS_FX.BASS_BFX_DAMP p = new BASS_FX.BASS_BFX_DAMP();
        BASS.BASS_FXGetParameters(effectMap.get(Effect.DAMP), p);

        p.fTarget = app.getRegistryFloat(App.DAMP_TARGET);
        p.fQuiet = app.getRegistryFloat(App.DAMP_QUIET);
        p.fGain = app.getRegistryFloat(App.DAMP_GAIN);
        p.fDelay = app.getRegistryFloat(App.DAMP_DELAY);
        p.fRate = app.getRegistryFloat(App.DAMP_RATE);

        if (app.getRegistryInt(App.DAMP) == 1) {
            p.lChannel = BASS_FX.BASS_BFX_CHANALL;
        } else {
            p.lChannel = BASS_FX.BASS_BFX_CHANNONE;
        }
        BASS.BASS_FXSetParameters(effectMap.get(Effect.DAMP), p);

    }

    public void enableVolume() {

//        removeEffect(Effect.VOLUME);
        if (effectMap.get(Effect.VOLUME) == null) {
            effectMap.put(Effect.VOLUME, BASS.BASS_ChannelSetFX(chan, BASS_FX.BASS_FX_BFX_VOLUME, 0));
        }
        BASS_FX.BASS_BFX_VOLUME p = new BASS_FX.BASS_BFX_VOLUME();
        BASS.BASS_FXGetParameters(effectMap.get(Effect.VOLUME), p);
        p.fVolume = (float) BASS_FX.BASS_BFX_dB2Linear(app.getRegistryFloat(App.VOLUME));
        p.lChannel = BASS_FX.BASS_BFX_CHANALL;

        BASS.BASS_FXSetParameters(effectMap.get(Effect.VOLUME), p);

    }

    public void enableChorus() {

        removeEffect(Effect.CHORUS);
        if (effectMap.get(Effect.CHORUS) == null) {
            effectMap.put(Effect.CHORUS, BASS.BASS_ChannelSetFX(chan, BASS_FX.BASS_FX_BFX_CHORUS, 0));
        }

        BASS_FX.BASS_BFX_CHORUS p = new BASS_FX.BASS_BFX_CHORUS();
        BASS.BASS_FXGetParameters(effectMap.get(Effect.CHORUS), p);

        p.fDryMix = app.getRegistryFloat(App.CHORUS_DRYMIX);
        p.fWetMix = app.getRegistryFloat(App.CHORUS_WETMIX);
        p.fFeedback = app.getRegistryFloat(App.CHORUS_FEEDBACK);
        p.fMinSweep = app.getRegistryFloat(App.CHORUS_MINSWEEP);
        p.fMaxSweep = app.getRegistryFloat(App.CHORUS_MAXSWEEP);
        p.fRate = app.getRegistryFloat(App.CHORUS_RATE);

        if (app.getRegistryInt(App.CHORUS) == 1) {
            p.lChannel = BASS_FX.BASS_BFX_CHANALL;
        } else {
            p.lChannel = BASS_FX.BASS_BFX_CHANNONE;
        }
        BASS.BASS_FXSetParameters(effectMap.get(Effect.CHORUS), p);

    }

    public void enableCompressor() {
//        removeEffect(Effect.COMPRESSOR);

        if (effectMap.get(Effect.COMPRESSOR) == null) {
            effectMap.put(Effect.COMPRESSOR, BASS.BASS_ChannelSetFX(chan, BASS_FX.BASS_FX_BFX_COMPRESSOR2, 0));
        }
        BASS_FX.BASS_BFX_COMPRESSOR2 p = new BASS_FX.BASS_BFX_COMPRESSOR2();
        BASS.BASS_FXGetParameters(effectMap.get(Effect.COMPRESSOR), p);
        p.fGain = app.getRegistryFloat(App.COMPRESSOR_GAIN);
        p.fThreshold = app.getRegistryFloat(App.COMPRESSOR_THRESHOLD);
        p.fRatio = app.getRegistryFloat(App.COMPRESSOR_RATIO);
        p.fAttack = app.getRegistryFloat(App.COMPRESSOR_ATTACK);
        p.fRelease = app.getRegistryFloat(App.COMPRESSOR_RELEASE);
        if (app.getRegistryInt(App.COMPRESSOR) == 1) {
            p.lChannel = BASS_FX.BASS_BFX_CHANALL;
        } else {
            p.lChannel = BASS_FX.BASS_BFX_CHANNONE;
        }

        BASS.BASS_FXSetParameters(effectMap.get(Effect.COMPRESSOR), p);

    }

    public void enableFreeverb() {

//        removeEffect(Effect.FREEVERB);
        if (effectMap.get(Effect.FREEVERB) == null) {
            effectMap.put(Effect.FREEVERB, BASS.BASS_ChannelSetFX(chan, BASS_FX.BASS_FX_BFX_FREEVERB, 0));
        }
        BASS_FX.BASS_BFX_FREEVERB p = new BASS_FX.BASS_BFX_FREEVERB();
        BASS.BASS_FXGetParameters(effectMap.get(Effect.FREEVERB), p);
        p.fDryMix = app.getRegistryFloat(App.FREEVERB_DRYMIX);
        p.fWetMix = app.getRegistryFloat(App.FREEVERB_WETMIX);
        p.fRoomSize = app.getRegistryFloat(App.FREEVERB_ROOMSIZE);
        p.fDamp = app.getRegistryFloat(App.FREEVERB_DAMP);
        p.fWidth = app.getRegistryFloat(App.FREEVERB_WIDTH);
        p.lMode = app.getRegistryInt(App.FREEVERB_MODE);
        if (app.getRegistryInt(App.FREEVERB) == 1) {
            p.lChannel = BASS_FX.BASS_BFX_CHANALL;
        } else {
            p.lChannel = BASS_FX.BASS_BFX_CHANNONE;
        }

        BASS.BASS_FXSetParameters(effectMap.get(Effect.FREEVERB), p);

    }

    public void enablePeaeq() {
        app.getCurrentEq();
        int i = 0;
        for (EqItem eqItem : app.getCurrentEq()) {
            int shift = Effect.PEAEQ_01;
            if (effectMap.get(i + shift) == null) {
                effectMap.put(i + shift, BASS.BASS_ChannelSetFX(chan, BASS_FX.BASS_FX_BFX_PEAKEQ, 0));
            }

            BASS_FX.BASS_BFX_PEAKEQ p = new BASS_FX.BASS_BFX_PEAKEQ();
            BASS.BASS_FXGetParameters(effectMap.get(i + shift), p);
            p.lBand = i;
            p.fCenter = eqItem.getFrequency();
            p.fQ = eqItem.getBandwidth();
            p.fGain = eqItem.getGain();
            if (app.getRegistryInt(App.PEAEQ) == 1) {
                p.lChannel = BASS_FX.BASS_BFX_CHANALL;
            } else {
                p.lChannel = BASS_FX.BASS_BFX_CHANNONE;
            }
            BASS.BASS_FXSetParameters(effectMap.get(i + shift), p);
            i++;
        }
//        for (int i = 0; i < 10; i++) {
//            int shift = Effect.PEAEQ_01;
////            removeEffect(i + shift);
//            if (effectMap.get(i + shift) == null) {
//                effectMap.put(i + shift, BASS.BASS_ChannelSetFX(chan, BASS_FX.BASS_FX_BFX_PEAKEQ, 0));
//            }
//            EqItem eqItem = app.getCurrentEq().get(i);
//            BASS_FX.BASS_BFX_PEAKEQ p = new BASS_FX.BASS_BFX_PEAKEQ();
//            BASS.BASS_FXGetParameters(effectMap.get(i + shift), p);
//            p.lBand = i;
//            p.fCenter = eqItem.getFrequency();
//            p.fQ = eqItem.getBandwidth();
//            p.fGain = eqItem.getGain();
//            if (app.getRegistryInt(App.PEAEQ) == 1) {
//                p.lChannel = BASS_FX.BASS_BFX_CHANALL;
//            } else {
//                p.lChannel = BASS_FX.BASS_BFX_CHANNONE;
//            }
//            BASS.BASS_FXSetParameters(effectMap.get(i + shift), p);
//        }
    }

    private void removeEffect(int key) {
        if (effectMap.get(key) != null) {
            BASS.BASS_ChannelRemoveFX(
                    chan,
                    effectMap.get(key)
            );
            effectMap.remove(key);
        }

    }

    public int getSecondsPosition() {
        return (int) BASS.BASS_ChannelBytes2Seconds(chan, getBytesPosition());
    }

    public void setSecondsPosition(int position) {
        setBytesPosition(BASS.BASS_ChannelSeconds2Bytes(chan, position));
    }

    public long getBytesPosition() {
        return BASS.BASS_ChannelGetPosition(chan, BASS.BASS_POS_BYTE);
    }

    public void setBytesPosition(long position) {
        BASS.BASS_ChannelSetPosition(chan, position, BASS.BASS_POS_BYTE);
    }

    public int getSecondsTotal() {
        return (int) BASS.BASS_ChannelBytes2Seconds(chan, getBytesTotal());
    }

    public long getBytesTotal() {
        return BASS.BASS_ChannelGetLength(chan, BASS.BASS_POS_BYTE);
    }

    public String getPositionTime() {
        String totalRes = secondsToString(getSecondsTotal());
        if (totalRes.startsWith("00:")) {
            return secondsToString(getSecondsPosition()).substring(3);
        }
        return secondsToString(getSecondsPosition());
    }

    public String getTotalTime() {
        String res = secondsToString(getSecondsTotal());
        if (res.startsWith("00:")) {
            return res.substring(3);
        }
        return secondsToString(getSecondsTotal());
    }

    private String secondsToString(Integer sec) {
        int h = 0;
        int m = 0;
        if (sec >= 3600) {
            h = (sec - (sec % 3600)) / 3600;
        }
        int hDev = sec - (h * 3600);
        if (hDev >= 60) {
            m = (hDev - (hDev % 60)) / 60;
        }
        int s = hDev - (m * 60);
        return repairDigit(h) + ":" + repairDigit(m) + ":" + repairDigit(s);

    }

    private String repairDigit(Integer dg) {
        if (dg < 10) {
            return "0" + dg;
        }
        return dg.toString();
    }

    public class Effect {
        public static final int FREEVERB = 0;

        public static final int PEAEQ_01 = 1;
        public static final int PEAEQ_02 = 2;
        public static final int PEAEQ_03 = 3;
        public static final int PEAEQ_04 = 4;
        public static final int PEAEQ_05 = 5;
        public static final int PEAEQ_06 = 6;
        public static final int PEAEQ_07 = 7;
        public static final int PEAEQ_08 = 8;
        public static final int PEAEQ_09 = 9;
        public static final int PEAEQ_10 = 10;

        public static final int CHORUS = 11;
        public static final int VOLUME = 12;
        public static final int COMPRESSOR = 13;
        public static final int DAMP = 14;
    }


}