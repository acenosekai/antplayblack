package com.acenosekai.antplay.sugar;

import android.media.MediaMetadataRetriever;

import com.acenosekai.antplay.App;
import com.acenosekai.antplay.R;
import com.acenosekai.antplay.realm.Album;
import com.acenosekai.antplay.realm.MediaLibrary;
import com.acenosekai.antplay.realm.MusicFile;

import java.io.File;

import io.realm.RealmResults;

//import com.acenosekai.antplay.realm.Cover;

/**
 * Created by Acenosekai on 12/9/2015.
 * Rock On
 */
public class LibraryGenerator {

    private App app;

    public LibraryGenerator(App app) {
        this.app = app;
    }

    public boolean reload() {
//        RealmResults<MediaLibrary> mmRes = app.getRealm().where(MediaLibrary.class).beginsWith("path", path).findAll();
//        app.getRealm().beginTransaction();
//        mmRes.clear();
//        app.getRealm().commitTransaction();

        RealmResults<MediaLibrary> mediaLibraryRealmResultsAll = app.getRealm().where(MediaLibrary.class).findAll();
        RealmResults<MusicFile> mmRes = app.getRealm().where(MusicFile.class).findAll();
        RealmResults<Album> alRes = app.getRealm().where(Album.class).findAll();
        app.getRealm().beginTransaction();
        mmRes.clear();
        alRes.clear();
        for (MediaLibrary mLib : mediaLibraryRealmResultsAll) {
            if (new File(mLib.getPath()).exists()) {
                generate(mLib.getPath());
            } else {
                mLib.removeFromRealm();
            }
        }
        app.getRealm().commitTransaction();
        return true;
    }

    public boolean add(String dir) {
        MediaLibrary mdl = new MediaLibrary();
        mdl.setPath(dir);
        RealmResults<MediaLibrary> mediaLibraryRealmResultsAll = app.getRealm().where(MediaLibrary.class).findAll();
        for (MediaLibrary mLib : mediaLibraryRealmResultsAll) {
            if (dir.startsWith(mLib.getPath())) {
                return false;
            }
        }
        RealmResults<MediaLibrary> mmRes = app.getRealm().where(MediaLibrary.class).beginsWith("path", dir).findAll();
        app.getRealm().beginTransaction();
        mmRes.clear();
        app.getRealm().copyToRealmOrUpdate(mdl);
        generate(dir);
        app.getRealm().commitTransaction();
        return true;
    }

    public boolean delete(String path) {
//        RealmResults<MusicFile> mfRes = app.getRealm().where(MusicFile.class).beginsWith("path", path).findAll();
        RealmResults<MediaLibrary> mmRes = app.getRealm().where(MediaLibrary.class).beginsWith("path", path).findAll();
//        Iterator<MusicFile> musicFileIterator = mfRes.iterator();
        app.getRealm().beginTransaction();
//        while (musicFileIterator.hasNext()) {
//            MusicFile mf = musicFileIterator.next();
//
//
//            RealmResults<MusicFile> musicAlbum = app.getRealm().where(MusicFile.class).equalTo("album.key", mf.getAlbum().getKey()).findAll();
//            if (musicAlbum.size() <= 1) {
//                RealmResults<Album> alRes = app.getRealm().where(Album.class).equalTo("key", mf.getAlbum().getKey()).findAll();
//                alRes.clear();
//            }
//            mf.removeFromRealm();
//        }
        mmRes.clear();
        app.getRealm().commitTransaction();
        reload();
        return true;
    }

    private void generate(String dir) {
        File f = new File(dir);
        if (f.listFiles() != null) {
            for (File fch : f.listFiles()) {
                if (//mp4 format
                        fch.getPath().toLowerCase().endsWith(".mp4")
                                || fch.getPath().toLowerCase().endsWith(".m4a")
                                || fch.getPath().toLowerCase().endsWith(".aac")
                                //flac format
                                || fch.getPath().toLowerCase().endsWith(".flac")
                                //mp3 format
                                || fch.getPath().toLowerCase().endsWith(".mp3")
                                //vorbis format
                                || fch.getPath().toLowerCase().endsWith(".ogg")
                                //PCM wave format
                                || fch.getPath().toLowerCase().endsWith(".wav")
                                //other
                                || fch.getPath().toLowerCase().endsWith(".aiff")
                                || fch.getPath().toLowerCase().endsWith(".ape")
                                || fch.getPath().toLowerCase().endsWith(".dff")
                                || fch.getPath().toLowerCase().endsWith(".dsf")
                                || fch.getPath().toLowerCase().endsWith(".opus")
                                || fch.getPath().toLowerCase().endsWith(".sf2")
                                || fch.getPath().toLowerCase().endsWith(".sfz")
                                || fch.getPath().toLowerCase().endsWith(".mpc")
                                || fch.getPath().toLowerCase().endsWith(".mpp")
                                || fch.getPath().toLowerCase().endsWith(".mp+")
                                || fch.getPath().toLowerCase().endsWith(".tta")
                        ) {
                    MusicFile musicFile = new MusicFile();
                    musicFile.setPath(fch.getPath());
                    musicFile.setTitle(fch.getName());
                    musicFile.setInitialized(false);
                    musicFile.setInvalid(false);
                    initializeMusicFile(musicFile);

                    if (musicFile.getArtist() == null || musicFile.getArtist().equals("")) {
                        musicFile.setArtist(app.getString(R.string.unknown_artist));
                    }

                    if (musicFile.getTitle() == null || musicFile.getTitle().equals("")) {
                        musicFile.setTitle(fch.getName());
                    }

                    if (musicFile.getAlbum() == null || musicFile.getAlbum().getName() == null || musicFile.getAlbum().getName().equals("")) {
                        Album album = new Album();
                        album.setName(app.getString(R.string.unknown_album));
                        album.setKey(app.getString(R.string.unknown_album));
                        album.setArtist(musicFile.getArtist());
                        app.getRealm().copyToRealmOrUpdate(album);

                        musicFile.setAlbum(album);
                    }
                    app.getRealm().copyToRealmOrUpdate(musicFile);
                }
                if (fch.isDirectory()) {
                    generate(fch.getPath());
                }
            }
        }
    }

    private void initializeMusicFile(MusicFile mf) {
        File fch = new File(mf.getPath());
        if (fch.getPath().endsWith(".mp4")
                || fch.getPath().endsWith(".m4a")
                || fch.getPath().endsWith(".aac")
                //flac format
                || fch.getPath().endsWith(".flac")
                //mp3 format
                || fch.getPath().endsWith(".mp3")
                //vorbis format
                || fch.getPath().endsWith(".ogg")
                //PCM wave format
                || fch.getPath().endsWith(".wav")) {
            try {
                MediaMetadataRetriever metaRetriver = new MediaMetadataRetriever();
                metaRetriver.setDataSource(fch.getPath());
                mf.setTitle(metaRetriver.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE));
                mf.setArtist(metaRetriver.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST));
                mf.setGenre(metaRetriver.extractMetadata(MediaMetadataRetriever.METADATA_KEY_GENRE));

                if (metaRetriver.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ALBUM) != null) {
                    Album album = new Album();

                    album.setName(metaRetriver.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ALBUM));
                    if (metaRetriver.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ALBUMARTIST) != null) {
                        album.setArtist(metaRetriver.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ALBUMARTIST));
                    } else {
                        album.setArtist(metaRetriver.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST));
                    }
                    album.setKey(album.getName() + album.getArtist());
                    if (metaRetriver.getEmbeddedPicture() != null) {
                        album.setCoverArt(metaRetriver.getEmbeddedPicture());
                    }

                    app.getRealm().copyToRealmOrUpdate(album);
                    mf.setAlbum(album);
                }

                mf.setInitialized(true);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }


}
