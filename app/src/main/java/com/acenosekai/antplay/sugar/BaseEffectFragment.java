package com.acenosekai.antplay.sugar;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import com.acenosekai.antplay.R;
import com.acenosekai.antplay.adapter.SliderAdapter;
import com.acenosekai.antplay.fragment.MainFragment;
import com.acenosekai.antplay.sugar.obj.SliderItem;

import net.soroushjavdan.customfontwidgets.CTextView;

import java.util.List;

/**
 * Created by Acenosekai on 1/2/2016.
 * Rock On
 */
abstract public class BaseEffectFragment extends BaseFragment {
    protected BaseFragment backFragment = new MainFragment();
    private View effectView;
    private RecyclerView contentList;
    private SliderAdapter adapter;

    @Override
    public BaseFragment getBackFragment() {
        return backFragment;
    }

    @Override
    public void setBackFragment(BaseFragment backFragment) {
        this.backFragment = backFragment;
    }

    abstract public List<SliderItem> getSliderItemList();

    abstract public int getFragmentTitle();

    abstract public boolean useFragmentSwitch();

    abstract public int getFragmentSwitchKey();

    public int getLayout() {
        return R.layout.fragment_effect;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        this.effectView = inflater.inflate(getLayout(), container, false);
        CTextView ctv = (CTextView) effectView.findViewById(R.id.fragment_title);
        ctv.setText(getFragmentTitle());
        SwitchCompat fragmentSwitch = (SwitchCompat) effectView.findViewById(R.id.fragment_switch);
        if (useFragmentSwitch()) {

            fragmentSwitch.setChecked(app.getRegistry(getFragmentSwitchKey()).equals("1"));
            fragmentSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        app.setRegistry(getFragmentSwitchKey(), "1");
                    } else {
                        app.setRegistry(getFragmentSwitchKey(), "0");
                    }
                }
            });
        } else {
            fragmentSwitch.setVisibility(View.GONE);
        }

        this.contentList = (RecyclerView) effectView.findViewById(R.id.content_list);
        LinearLayoutManager llm = new LinearLayoutManager(activity);
        contentList.setLayoutManager(llm);
        contentList.setHasFixedSize(true);

        this.adapter = new SliderAdapter(activity, getSliderItemList());
        contentList.setAdapter(adapter);

        return effectView;
    }
}
