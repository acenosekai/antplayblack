package com.acenosekai.antplay.realm;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class PlayList extends RealmObject {
    @PrimaryKey
    private String name;
    private int type;
    private RealmList<MusicFile> musicFileList;

    public RealmList<MusicFile> getMusicFileList() {
        return musicFileList;
    }

    public void setMusicFileList(RealmList<MusicFile> musicFileList) {
        this.musicFileList = musicFileList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
