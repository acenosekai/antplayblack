package com.acenosekai.antplay.realm;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class AppRegistry extends RealmObject {
    @PrimaryKey
    private int key;
    private String val;
    private String type;

    public int getKey() {
        return key;
    }

    public void setKey(int key) {
        this.key = key;
    }

    public String getVal() {
        return val;
    }

    public void setVal(String val) {
        this.val = val;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
