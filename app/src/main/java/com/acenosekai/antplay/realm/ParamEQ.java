package com.acenosekai.antplay.realm;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;


public class ParamEQ extends RealmObject {
    @PrimaryKey
    private String presetName;
    private String eqString;
    private boolean system;

    public String getPresetName() {
        return presetName;
    }

    public void setPresetName(String presetName) {
        this.presetName = presetName;
    }

    public String getEqString() {
        return eqString;
    }

    public void setEqString(String eqString) {
        this.eqString = eqString;
    }

    public boolean getSystem() {
        return system;
    }

    public void setSystem(boolean system) {
        this.system = system;
    }
}
