package com.acenosekai.antplay.realm;


import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class MediaLibrary extends RealmObject {
    @PrimaryKey
    private String path;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

}
