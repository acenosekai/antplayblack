package com.acenosekai.antplay.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.acenosekai.antplay.R;

import net.soroushjavdan.customfontwidgets.CTextView;

import java.util.List;

/**
 * Created by Acenosekai on 12/7/2015.
 * Rock On
 */
public class DirectoryAdapter extends ArrayAdapter<String> {
    Context mContext;
    private List<String> data;

    public DirectoryAdapter(Context context, int resource, List<String> objects) {
        super(context, resource, objects);
        this.mContext = context;
        this.data = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        String objectItem = data.get(position);// data[position];
        if (convertView == null) {
            // inflate the layout
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            convertView = inflater.inflate(R.layout.item_directory, parent, false);
        }
        CTextView ctv = (CTextView) convertView.findViewById(R.id.directory_name);
        ctv.setText(objectItem);
        return convertView;
    }
}
