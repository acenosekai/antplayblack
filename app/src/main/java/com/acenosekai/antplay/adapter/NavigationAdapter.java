package com.acenosekai.antplay.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.acenosekai.antplay.R;
import com.acenosekai.antplay.objView.Navigation;

import net.soroushjavdan.customfontwidgets.CTextView;

import java.util.List;

/**
 * Created by Acenosekai on 12/6/2015.
 * Rock On
 */
public class NavigationAdapter<T extends Navigation> extends ArrayAdapter<T> {
    Context mContext;

    private List<T> data;

    public NavigationAdapter(Context context, int resource, List<T> objects) {
        super(context, resource, objects);
        this.mContext = context;
        this.data = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        T objectItem = data.get(position);// data[position];
        // inflate the layout
        LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
        convertView = inflater.inflate(objectItem.getLayoutId(), parent, false);
        objectItem.onBind(mContext, convertView);
        try {
            CTextView ctv = (CTextView) convertView.findViewById(R.id.navigation_title);
            ctv.setText(objectItem.getName());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertView;
    }
}
