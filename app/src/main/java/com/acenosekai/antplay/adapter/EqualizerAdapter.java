package com.acenosekai.antplay.adapter;

import android.graphics.Point;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;

import com.acenosekai.antplay.App;
import com.acenosekai.antplay.BaseActivity;
import com.acenosekai.antplay.R;
import com.acenosekai.antplay.objView.SliderEqHolder;
import com.acenosekai.antplay.sugar.ExtendedSeekBar;
import com.acenosekai.antplay.sugar.obj.EqItem;
import com.rey.material.app.Dialog;

import java.util.List;

/**
 * Created by Acenosekai on 1/4/2016.
 * Rock On
 */
public class EqualizerAdapter extends RecyclerView.Adapter<SliderEqHolder> {

    private List<EqItem> eqItemList;
    private BaseActivity activity;
    private App app;

    public EqualizerAdapter(BaseActivity activity) {

        this.activity = activity;
        this.app = (App) activity.getApplication();
        this.eqItemList = app.getCurrentEq();

        app.setOnEqualizerChange(new App.OnEqualizerChange() {
            @Override
            public void onEqualizerChange() {
//                try {
                eqItemList = app.getCurrentEq();
                notifyDataSetChanged();
//                }catch(Exception e){}
            }
        });
    }

    @Override
    public SliderEqHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_slider_eq, parent, false);
        return new SliderEqHolder(v);
    }

    @Override
    public void onBindViewHolder(final SliderEqHolder holder, final int position) {
        final EqItem item = eqItemList.get(position);
        ExtendedSeekBar exSlider = new ExtendedSeekBar(holder.getSlider(), -15, 15, 60);

        exSlider.setValue(item.getGain());
        exSlider.setOnExtendedSeekBarChangeListener(new ExtendedSeekBar.OnExtendedSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, float value, boolean fromUser) {
                if (fromUser) {
                    holder.getValue().setText(value + "db");
                    item.setGain(value);
                    app.saveEq(eqItemList);
                }
            }
        });
        holder.getLabel().setText(((int) item.getFrequency()) + "Hz");
        holder.getValue().setText(item.getGain() + "db");

        holder.getSetting().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Point size = new Point();
                WindowManager w = activity.getWindowManager();
                w.getDefaultDisplay().getSize(size);

                final Dialog settingEq = new Dialog(activity);
                final View dialogEqSettingView = activity.getLayoutInflater().inflate(R.layout.dialog_equalizer, null);

                SeekBar q = (SeekBar) dialogEqSettingView.findViewById(R.id.slider_eq);


                final ExtendedSeekBar exQ = new ExtendedSeekBar(q, 0, 1, 40);
                exQ.setValue(item.getBandwidth());
                final EditText freqText = (EditText) dialogEqSettingView.findViewById(R.id.dialog_equalizer_frequency);
                freqText.setText(String.valueOf((int) item.getFrequency()));


                ((TextView) dialogEqSettingView.findViewById(R.id.slider_label_eq)).setText(R.string.equalizer_q);
                ((TextView) dialogEqSettingView.findViewById(R.id.slider_value_eq)).setText(String.valueOf(exQ.getValue()));

                exQ.setOnExtendedSeekBarChangeListener(new ExtendedSeekBar.OnExtendedSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, float value, boolean fromUser) {
                        ((TextView) dialogEqSettingView.findViewById(R.id.slider_value_eq)).setText(String.valueOf(value));
                    }
                });

                settingEq.applyStyle(R.style.Material_App_Dialog).negativeAction(R.string.ant_dialog_cancel)
                        .title(R.string.dialog_equalizer_setting_title)
                        .contentView(dialogEqSettingView)
                        .positiveAction(R.string.ant_dialog_ok)
                        .positiveActionClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                item.setBandwidth(exQ.getValue());
                                int freq = Integer.parseInt(freqText.getText().toString());
                                if (freq < 20) {
                                    freq = 20;
                                } else if (freq > 20000) {
                                    freq = 20000;
                                }
                                item.setFrequency(freq);
                                app.saveEq(eqItemList);
                                settingEq.hide();
                            }
                        })
                        .negativeActionClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                settingEq.hide();
                            }
                        })
                        .cancelable(true)
                        .canceledOnTouchOutside(true)
                        .layoutParams(((int) (size.x * 0.8)), 600).show();
            }
        });


    }

    @Override
    public int getItemCount() {
        return eqItemList.size();
    }
}
