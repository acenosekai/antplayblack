package com.acenosekai.antplay.adapter;

import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.acenosekai.antplay.BaseActivity;
import com.acenosekai.antplay.R;
import com.acenosekai.antplay.objView.FilesViewHolder;
import com.acenosekai.antplay.realm.MusicFile;
import com.mikepenz.community_material_typeface_library.CommunityMaterial;

import io.realm.RealmList;

/**
 * Created by Acenosekai on 12/27/2015.
 * Rock On
 */
public class QueueAdapter extends RecyclerView.Adapter<FilesViewHolder> {
    private BaseActivity activity;

    public QueueAdapter(BaseActivity activity) {
        this.activity = activity;
    }

    @Override
    public FilesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_files, parent, false);
        return new FilesViewHolder(v);
    }

    @Override
    public void onBindViewHolder(FilesViewHolder holder, final int position) {
        RealmList<MusicFile> musicList = activity.getPlaybackService().getMusicList();
        int pos = activity.getPlaybackService().getActualPos();
        MusicFile musicFile = musicList.get(position);
        holder.getName().setText(musicFile.getTitle());
        holder.getDescription().setText(musicFile.getArtist() + " - " + musicFile.getAlbum().getName());
        if (pos == position) {
            if (activity.getPlaybackService().isPlaying()) {
                holder.getListIcon().setIcon(CommunityMaterial.Icon.cmd_play_circle);
            } else {
                holder.getListIcon().setIcon(CommunityMaterial.Icon.cmd_pause_circle);
            }

            holder.getItemView().setBackgroundColor(ContextCompat.getColor(activity, R.color.colorAccent));
        } else {
            holder.getListIcon().setIcon(CommunityMaterial.Icon.cmd_music_circle);
            holder.getItemView().setBackgroundColor(ContextCompat.getColor(activity, R.color.colorBlack));
        }
        holder.getItemView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.getPlaybackService().getActualPos();

                if (activity.getPlaybackService().getActualPos() == position && activity.getPlaybackService().isPlaying()) {
                    activity.getPlaybackService().playbackPause();
                } else if (activity.getPlaybackService().getActualPos() == position && !activity.getPlaybackService().isPlaying()) {
                    activity.getPlaybackService().playbackPlay(true);
                } else {
                    activity.getPlaybackService().playbackTo(position);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return activity.getPlaybackService().getMusicList().size();
    }
}
