package com.acenosekai.antplay.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;

import com.acenosekai.antplay.App;
import com.acenosekai.antplay.BaseActivity;
import com.acenosekai.antplay.R;
import com.acenosekai.antplay.objView.SliderHolder;
import com.acenosekai.antplay.sugar.ExtendedSeekBar;
import com.acenosekai.antplay.sugar.obj.SliderItem;

import java.text.DecimalFormat;
import java.util.List;

/**
 * Created by Acenosekai on 1/1/2016.
 * Rock On
 */
public class SliderAdapter extends RecyclerView.Adapter<SliderHolder> {

    private List<SliderItem> sliderItemList;
    private BaseActivity activity;
    private App app;

    public SliderAdapter(BaseActivity activity, List<SliderItem> sliderItemList) {
        this.sliderItemList = sliderItemList;
        this.activity = activity;
        this.app = (App) activity.getApplication();
    }

    @Override
    public SliderHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_slider, parent, false);
        return new SliderHolder(v);
    }

    //min -15
    //max 10


    @Override
    public void onBindViewHolder(final SliderHolder holder, int position) {
        final SliderItem item = sliderItemList.get(position);

        holder.getLabel().setText(item.getLabel());
        holder.getValue().setText(generateValue(item));
        ExtendedSeekBar exSlider = new ExtendedSeekBar(holder.getSlider(), item.getMin(), item.getMax(), item.getStep());
        exSlider.setValue(item.getValue());
        exSlider.setOnExtendedSeekBarChangeListener(new ExtendedSeekBar.OnExtendedSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, float value, boolean fromUser) {
                item.setValue(value);
                holder.getValue().setText(generateValue(item));
                if (item.getOnPositionChangeListener() != null) {
                    item.getOnPositionChangeListener().onPositionChanged(value);
                }
            }
        });
    }

    private String generateValue(SliderItem item) {
        DecimalFormat df = new DecimalFormat();
        df.setMaximumFractionDigits(2);
        if (item.getSuffix() == null) {
            return df.format(item.getValue());
        } else {
            return df.format(item.getValue()) + " " + item.getSuffix();
        }
    }


    @Override
    public int getItemCount() {
        return sliderItemList.size();
    }
}
