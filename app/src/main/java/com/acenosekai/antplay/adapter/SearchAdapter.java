package com.acenosekai.antplay.adapter;

import com.acenosekai.antplay.BaseActivity;
import com.acenosekai.antplay.fragment.SearchFragment;
import com.acenosekai.antplay.objView.FilesViewHolder;
import com.acenosekai.antplay.realm.Album;
import com.acenosekai.antplay.realm.MusicFile;
import com.acenosekai.antplay.sugar.FilesView;
import com.acenosekai.antplay.sugar.obj.Artist;

import java.util.List;

/**
 * Created by Acenosekai on 12/12/2015.
 * Rock On
 */
public class SearchAdapter extends FilesBaseAdapter {
    private List<Object> searchObj;
    private String searchText;

    public SearchAdapter(BaseActivity activity, List<Object> searchObj, String searchText) {
        super(activity);
        this.searchObj = searchObj;
        this.searchText = searchText;
    }

    @Override
    public void onBindViewHolder(FilesViewHolder holder, int position) {
        final Object objectItem = searchObj.get(position);// data[position];
        SearchFragment sf = new SearchFragment();
        sf.setSearchText(searchText);

        try {
            MusicFile musicFile = (MusicFile) objectItem;
            new FilesView(activity, holder).musicView(musicFile);
        } catch (Exception e) {
            try {
                Album album = (Album) objectItem;
                new FilesView(activity, holder).albumView(album, sf);
            } catch (Exception e1) {
                try {
                    Artist artist = (Artist) objectItem;
                    new FilesView(activity, holder).artistView(artist, sf);
                } catch (Exception e2) {
                    //other
                }

            }
        }
    }

    @Override
    public int getItemCount() {
        return searchObj.size();
    }
}
