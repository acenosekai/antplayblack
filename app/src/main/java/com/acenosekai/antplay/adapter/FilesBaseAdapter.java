package com.acenosekai.antplay.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.acenosekai.antplay.App;
import com.acenosekai.antplay.BaseActivity;
import com.acenosekai.antplay.R;
import com.acenosekai.antplay.fragment.AlbumFragment;
import com.acenosekai.antplay.fragment.ArtistFragment;
import com.acenosekai.antplay.fragment.PlayListBaseFragment;
import com.acenosekai.antplay.objView.FilesViewHolder;
import com.acenosekai.antplay.realm.Album;
import com.acenosekai.antplay.realm.MusicFile;
import com.acenosekai.antplay.realm.PlayList;
import com.acenosekai.antplay.sugar.FilesView;
import com.acenosekai.antplay.sugar.obj.Artist;

import java.util.List;

/**
 * Created by Acenosekai on 12/12/2015.
 * Rock On
 */
public class FilesBaseAdapter extends RecyclerView.Adapter<FilesViewHolder> {

    protected BaseActivity activity;
    protected App app;
    protected List<?> listItem;

    public FilesBaseAdapter(BaseActivity activity, List<?> listItem) {
        this.activity = activity;
        this.listItem = listItem;
    }

    public FilesBaseAdapter(BaseActivity activity) {
        this.activity = activity;
        this.app = (App) activity.getApplication();
    }

    @Override
    public void onBindViewHolder(FilesViewHolder holder, int position) {
        try {
            new FilesView(activity, holder).albumView((Album) listItem.get(position), new AlbumFragment());
        } catch (Exception e) {
            try {
                new FilesView(activity, holder).artistView((Artist) listItem.get(position), new ArtistFragment());
            } catch (Exception e2) {
                try {
                    new FilesView(activity, holder).musicView((MusicFile) listItem.get(position));
                } catch (Exception e3) {
                    try {
                        new FilesView(activity, holder).playlistView((PlayList) listItem.get(position), new PlayListBaseFragment());
                    } catch (Exception e4) {
                        e4.printStackTrace();
                    }
                }
            }

        }
    }

    @Override
    public int getItemCount() {
        return listItem.size();
    }

    @Override
    public FilesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_files, parent, false);
        return new FilesViewHolder(v);
    }

}
