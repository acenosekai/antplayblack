package com.acenosekai.antplay.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.acenosekai.antplay.App;
import com.acenosekai.antplay.BaseActivity;
import com.acenosekai.antplay.R;
import com.acenosekai.antplay.realm.ParamEQ;
import com.rey.material.app.Dialog;

import net.soroushjavdan.customfontwidgets.CTextView;

import io.realm.RealmResults;

/**
 * Created by Acenosekai on 1/4/2016.
 * Rock On
 */
public class EqPresetAdapter extends RecyclerView.Adapter<EqPresetAdapter.EqPresetHolder> {


    private BaseActivity activity;
    private RealmResults<ParamEQ> paramEQList;
    private App app;
    private Dialog dialog;

    public EqPresetAdapter(BaseActivity activity, RealmResults<ParamEQ> paramEQList, Dialog dialog) {
        this.activity = activity;
        this.paramEQList = paramEQList;
        this.dialog = dialog;
        this.app = (App) activity.getApplication();
    }

    @Override
    public EqPresetHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_string, parent, false);
        return new EqPresetHolder(v);
    }

    @Override
    public void onBindViewHolder(EqPresetHolder holder, int position) {
        final ParamEQ item = paramEQList.get(position);
        holder.name.setText(item.getPresetName());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                app.setRegistry(App.PEAEQ_STRING, item.getEqString());
                dialog.hide();
            }
        });
    }

    @Override
    public int getItemCount() {
        return paramEQList.size();
    }

    public class EqPresetHolder extends RecyclerView.ViewHolder {
        View itemView;
        CTextView name;

        public EqPresetHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
            this.name = (CTextView) itemView.findViewById(R.id.play_list_name);


        }
    }
}
