package com.acenosekai.antplay.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.acenosekai.antplay.fragment.NavigationFragment;
import com.acenosekai.antplay.fragment.NowplayingFragment;
import com.acenosekai.antplay.fragment.QueueFragment;

import java.util.List;

/**
 * Created by Acenosekai on 12/24/2015.
 * Rock On
 */
public class MainPagerAdapter  extends FragmentPagerAdapter {
    List<Fragment> fragments;

    public MainPagerAdapter(FragmentManager fm, List<Fragment> fragments) {
        super(fm);
        this.fragments = fragments;
    }

    @Override
    public Fragment getItem(int index) {
        return fragments.get(index);
//        switch (index) {
//            case 0:
//                // Top Rated fragment activity
//                return new NavigationFragment();
//            case 1:
//                // Games fragment activity
//                return new NowplayingFragment();
//            case 2:
////                // Movies fragment activity
//                return new QueueFragment();
//        }

//        return null;
    }

    @Override
    public int getCount() {
        // get item count - equal to number of tabs
        return fragments.size();
    }
}