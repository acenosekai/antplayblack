package com.acenosekai.antplay.adapter;

import com.acenosekai.antplay.BaseActivity;
import com.acenosekai.antplay.fragment.FolderFragment;
import com.acenosekai.antplay.objView.FilesViewHolder;
import com.acenosekai.antplay.sugar.FilesView;
import com.acenosekai.antplay.sugar.obj.Folder;

import java.util.List;

/**
 * Created by Acenosekai on 12/9/2015.
 * Rock On
 */
public class FolderAdapter extends FilesBaseAdapter {
    private List<Folder> folders;
    private FolderFragment folderFragment;

    public FolderAdapter(BaseActivity activity, List<Folder> folders, FolderFragment folderFragment) {
        super(activity);
        this.folders = folders;
        this.folderFragment = folderFragment;

    }
    @Override
    public void onBindViewHolder(FilesViewHolder holder, int position) {
        new FilesView(activity, holder).folderView(folders.get(position), folderFragment);
    }

    @Override
    public int getItemCount() {
        return folders.size();
    }

}
