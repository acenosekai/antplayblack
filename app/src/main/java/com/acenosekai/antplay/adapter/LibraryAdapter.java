package com.acenosekai.antplay.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.acenosekai.antplay.App;
import com.acenosekai.antplay.BaseActivity;
import com.acenosekai.antplay.R;
import com.acenosekai.antplay.realm.MediaLibrary;
import com.acenosekai.antplay.sugar.LibraryGenerator;
import com.mikepenz.iconics.view.IconicsImageView;

import net.soroushjavdan.customfontwidgets.CTextView;

import java.util.List;

/**
 * Created by Acenosekai on 12/9/2015.
 * Rock On
 */
public class LibraryAdapter extends RecyclerView.Adapter<LibraryAdapter.DirectoryViewHolder> {

    private List<MediaLibrary> mediaLibraries;
    private BaseActivity activity;
    private App app;

    public LibraryAdapter(BaseActivity activity, List<MediaLibrary> mediaLibraries) {
        this.mediaLibraries = mediaLibraries;
        this.activity = activity;
        this.app = (App) activity.getApplication();
    }

    @Override
    public DirectoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_library, parent, false);
        return new DirectoryViewHolder(v);
    }

    @Override
    public void onBindViewHolder(DirectoryViewHolder holder, int position) {
        final MediaLibrary objectItem = mediaLibraries.get(position);// data[position];
        holder.name.setText(objectItem.getPath());
        holder.iconDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LibraryGenerator libraryGenerator = new LibraryGenerator(app);
                libraryGenerator.delete(objectItem.getPath());
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mediaLibraries.size();
    }

    public static class DirectoryViewHolder extends RecyclerView.ViewHolder {
        View itemView;
        CTextView name;
        IconicsImageView iconDelete;

        public DirectoryViewHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
            this.name = (CTextView) itemView.findViewById(R.id.directory_name);
            this.iconDelete = (IconicsImageView) itemView.findViewById(R.id.library_delete);


        }
    }
}
