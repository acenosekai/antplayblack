package com.acenosekai.antplay.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.acenosekai.antplay.App;
import com.acenosekai.antplay.BaseActivity;
import com.acenosekai.antplay.R;
import com.acenosekai.antplay.realm.MusicFile;
import com.acenosekai.antplay.realm.PlayList;
import com.rey.material.app.Dialog;

import net.soroushjavdan.customfontwidgets.CTextView;

import java.util.List;

import io.realm.RealmList;

/**
 * Created by Acenosekai on 12/15/2015.
 * Rock On
 */
public class PlayListDialogAdapter extends RecyclerView.Adapter<PlayListDialogAdapter.DialogPlaylistViewHolder> {

    private List<PlayList> data;
    private BaseActivity activity;
    private List<MusicFile> musicList;
    private Dialog toPlaylistDialog;

    public PlayListDialogAdapter(BaseActivity activity, List<PlayList> data, List<MusicFile> musicList, Dialog toPlaylistDialog) {
        this.data = data;
        this.activity = activity;
        this.musicList = musicList;
        this.toPlaylistDialog = toPlaylistDialog;
    }

    @Override
    public DialogPlaylistViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_string, parent, false);
        return new DialogPlaylistViewHolder(v);
    }

    @Override
    public void onBindViewHolder(DialogPlaylistViewHolder holder, int position) {
        holder.name.setText(data.get(position).getName());
        final PlayList p = data.get(position);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                App app = (App) activity.getApplication();
                app.getRealm().beginTransaction();
                if (p.getMusicFileList() == null) {
                    p.setMusicFileList(new RealmList<MusicFile>());
                }
                p.getMusicFileList().addAll(musicList);
                app.getRealm().copyToRealmOrUpdate(p);
                app.getRealm().commitTransaction();
                toPlaylistDialog.hide();
                activity.setMultiSelectMode(false);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public static class DialogPlaylistViewHolder extends RecyclerView.ViewHolder {
        View itemView;
        CTextView name;

        public DialogPlaylistViewHolder(View itemView) {
            super(itemView);
            this.itemView = itemView;
            this.name = (CTextView) itemView.findViewById(R.id.play_list_name);


        }
    }
}
