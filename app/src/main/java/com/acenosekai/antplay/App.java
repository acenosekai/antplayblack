package com.acenosekai.antplay;

import android.app.Application;
import android.content.Intent;
import android.util.Log;

import com.acenosekai.antplay.realm.AppRegistry;
import com.acenosekai.antplay.realm.ParamEQ;
import com.acenosekai.antplay.realm.PlayList;
import com.acenosekai.antplay.services.PlaybackService;
import com.acenosekai.antplay.sugar.obj.EqItem;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.un4seen.bass.BASS_FX;

import net.soroushjavdan.customfontwidgets.FontUtils;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;

/**
 * Created by Acenosekai on 12/6/2015.
 * Rock On
 */
public class App extends Application {
    public static final String TAG = "antApp";
    public static final int PLAYLIST = 0;
    public static final int SONG_POSITION = 1;
    public static final int REPEAT = 2;
    public static final int SHUFFLE = 3;
    public static final int CROSSFADE = 4;
    public static final int CROSSTIME = 5;
    public static final int GAIN = 6;
    public static final int FREEVERB = 7;
    public static final int FREEVERB_DRYMIX = 8;
    public static final int FREEVERB_WETMIX = 9;
    public static final int FREEVERB_ROOMSIZE = 10;
    public static final int FREEVERB_DAMP = 11;
    public static final int FREEVERB_WIDTH = 12;
    public static final int FREEVERB_MODE = 13;
    public static final int PEAEQ = 14;
    public static final int PEAEQ_STRING = 15;
    public static final int CHORUS = 16;
    public static final int CHORUS_DRYMIX = 17;
    public static final int CHORUS_WETMIX = 18;
    public static final int CHORUS_FEEDBACK = 19;
    public static final int CHORUS_MINSWEEP = 20;
    public static final int CHORUS_MAXSWEEP = 21;
    public static final int CHORUS_RATE = 22;
    public static final int VOLUME = 23;
    public static final int COMPRESSOR = 24;
    public static final int COMPRESSOR_GAIN = 25;
    public static final int COMPRESSOR_THRESHOLD = 26;
    public static final int COMPRESSOR_RATIO = 27;
    public static final int COMPRESSOR_ATTACK = 28;
    public static final int COMPRESSOR_RELEASE = 29;
    public static final int DAMP = 30;
    public static final int DAMP_TARGET = 31;
    public static final int DAMP_QUIET = 32;
    public static final int DAMP_RATE = 33;
    public static final int DAMP_GAIN = 34;
    public static final int DAMP_DELAY = 35;
    public static final int CONFIG_SAMPLE_RATE = 36;
    public static final int CONFIG_BUFFER = 37;
    public static final int CONFIG_SRC = 38;
    public static final int CONFIG_FLOATDSP = 39;

    private Realm realm;
    private HashMap<Integer, String> registryBuffer = new HashMap<>();
    private OnRegistryChange onRegistryChange;
    private OnEqualizerChange onEqualizerChange;

    @Override
    public void onCreate() {
        super.onCreate();
        FontUtils.createFonts(this, "fonts");
        FontUtils.setDefaultFont(this, "fonts", "RightWay");

        RealmConfiguration config2 = new RealmConfiguration.Builder(this)
                .name("antPlayerDb")
                .schemaVersion(0)
                .deleteRealmIfMigrationNeeded()
                .build();
        realm = Realm.getInstance(config2);


        PlayList p = realm.where(PlayList.class).equalTo("name", "__defaultPlaylist").findFirst();

        realm.beginTransaction();
        if (p == null) {
            p = new PlayList();
            p.setName("__defaultPlaylist");
            p.setType(1);
            realm.copyToRealmOrUpdate(p);
        }

        if (realm.where(ParamEQ.class).equalTo("system", true).findAll().size() < 27) {
            realm.copyToRealmOrUpdate(listItemToParamEq("Flat", eqFlat()));
            realm.copyToRealmOrUpdate(listItemToParamEq("Acoustic", eqAcoustic()));
            realm.copyToRealmOrUpdate(listItemToParamEq("Bass", eqBass()));
            realm.copyToRealmOrUpdate(listItemToParamEq("Classical", eqClassical()));
            realm.copyToRealmOrUpdate(listItemToParamEq("Club", eqClub()));
            realm.copyToRealmOrUpdate(listItemToParamEq("Dance", eqDance()));
            realm.copyToRealmOrUpdate(listItemToParamEq("Disco", eqDisco()));
            realm.copyToRealmOrUpdate(listItemToParamEq("Electronic", eqElectronic()));
            realm.copyToRealmOrUpdate(listItemToParamEq("Full Bass & Treble", eqFullBassTreble()));
            realm.copyToRealmOrUpdate(listItemToParamEq("Full Bass", eqFullBass()));
            realm.copyToRealmOrUpdate(listItemToParamEq("Full Treble", eqFullTreble()));
            realm.copyToRealmOrUpdate(listItemToParamEq("Headphones", eqHeadphones()));
            realm.copyToRealmOrUpdate(listItemToParamEq("Hip Hop", eqHipHop()));
            realm.copyToRealmOrUpdate(listItemToParamEq("Jazz", eqJazz()));
            realm.copyToRealmOrUpdate(listItemToParamEq("Large Hall", eqLargeHall()));
            realm.copyToRealmOrUpdate(listItemToParamEq("Live", eqLive()));
            realm.copyToRealmOrUpdate(listItemToParamEq("Lounge", eqLounge()));
            realm.copyToRealmOrUpdate(listItemToParamEq("Party", eqParty()));
            realm.copyToRealmOrUpdate(listItemToParamEq("Pop", eqPop()));
            realm.copyToRealmOrUpdate(listItemToParamEq("R&B", eqRNB()));
            realm.copyToRealmOrUpdate(listItemToParamEq("Reggae", eqReggae()));
            realm.copyToRealmOrUpdate(listItemToParamEq("Rock", eqRock()));
            realm.copyToRealmOrUpdate(listItemToParamEq("Ska", eqSka()));
            realm.copyToRealmOrUpdate(listItemToParamEq("Soft Rock", eqSoftRock()));
            realm.copyToRealmOrUpdate(listItemToParamEq("Soft", eqSoft()));
            realm.copyToRealmOrUpdate(listItemToParamEq("Techno", eqTechno()));
            realm.copyToRealmOrUpdate(listItemToParamEq("Vocal", eqVocal()));
        }

        realm.commitTransaction();
//        Toast.makeText(App.this, realm.where(ParamEQ.class).equalTo("system", true).findAll().size(), Toast.LENGTH_SHORT).show();
        Log.i(TAG, String.valueOf(realm.where(ParamEQ.class).equalTo("system", true).findAll().size()));

    }

    private ParamEQ listItemToParamEq(String name, String eq) {
        ParamEQ pe = new ParamEQ();
        pe.setEqString(eq);
        pe.setSystem(true);
        pe.setPresetName(name);
        return pe;
    }

    public List<EqItem> getCurrentEq() {
        Gson gson = new Gson();
        Type listType = new TypeToken<List<EqItem>>() {
        }.getType();
        return gson.fromJson(getRegistry(App.PEAEQ_STRING), listType);
    }

    public String getRegistry(int key) {
        //cek on buffer map
        if (registryBuffer.get(key) != null) {
            return registryBuffer.get(key);
        }
        RealmResults<AppRegistry> appRegistry = realm.where(AppRegistry.class).equalTo("key", key).findAll();
        //create value
        String val = "";
        Boolean needSave = true;
        if (appRegistry.isEmpty()) {
            switch (key) {
                case App.PLAYLIST:
                    val = "__defaultPlaylist";
                    break;
                case App.SONG_POSITION:
                    val = "0";
                    break;
                case App.REPEAT:
                    val = String.valueOf(PlaybackService.Repeat.NO_REPEAT);
                    break;
                case App.SHUFFLE:
                    val = "0";
                    break;
                case App.CROSSFADE:
                    val = "0";
                    break;
                case App.CROSSTIME:
                    val = "10000";
                    break;
                case App.GAIN:
                    val = "1";
                    break;
                case App.FREEVERB:
                    val = "0";
                    break;
                case App.FREEVERB_DRYMIX:
                    val = "0";
                    break;
                case App.FREEVERB_WETMIX:
                    val = "1.0";
                    break;
                case App.FREEVERB_ROOMSIZE:
                    val = "0.5";
                    break;
                case App.FREEVERB_DAMP:
                    val = "0.5";
                    break;
                case App.FREEVERB_WIDTH:
                    val = "1";
                    break;
                case App.FREEVERB_MODE:
                    val = "0";
                    break;
                case App.PEAEQ:
                    val = "0";
                    break;
                case App.PEAEQ_STRING:
                    val = eqFlat();
                    break;
                case App.CHORUS:
                    val = "0";
                    break;
                case App.CHORUS_DRYMIX:
                    val = "0.0";
                    break;
                case App.CHORUS_WETMIX:
                    val = "0.4";
                    break;
                case App.CHORUS_FEEDBACK:
                    val = "0.0";
                    break;
                case App.CHORUS_MINSWEEP:
                    val = "200";
                    break;
                case App.CHORUS_MAXSWEEP:
                    val = "1000";
                    break;
                case App.CHORUS_RATE:
                    val = "500";
                    break;
                case App.VOLUME:
                    val = "0";
                    break;
                case App.COMPRESSOR:
                    val = "0";
                    break;
                case App.COMPRESSOR_GAIN:
                    val = "2";
                    break;
                case App.COMPRESSOR_THRESHOLD:
                    val = "-16";
                    break;
                case App.COMPRESSOR_RATIO:
                    val = "4";
                    break;
                case App.COMPRESSOR_ATTACK:
                    val = "2.2";
                    break;
                case App.COMPRESSOR_RELEASE:
                    val = "800";
                    break;
                case App.DAMP:
                    val = "0";
                    break;
                case App.DAMP_TARGET:
                    val = "0.92";
                    break;
                case App.DAMP_QUIET:
                    val = "0.02";
                    break;
                case App.DAMP_RATE:
                    val = "0.01";
                    break;
                case App.DAMP_GAIN:
                    val = String.valueOf(BASS_FX.BASS_BFX_dB2Linear(1));
                    break;
                case App.DAMP_DELAY:
                    val = "1";
                    break;
                case App.CONFIG_SAMPLE_RATE:
                    val = "44100";
                    break;
                case App.CONFIG_BUFFER:
                    val = "500";
                    break;
                case App.CONFIG_SRC:
                    val = "1";
                    break;
                case App.CONFIG_FLOATDSP:
                    val = "1";
                    break;

            }

            if (needSave) {
                AppRegistry ar = new AppRegistry();
                ar.setKey(key);
                ar.setVal(val);

                realm.beginTransaction();
                realm.copyToRealmOrUpdate(ar);
                realm.commitTransaction();
            }
            registryBuffer.put(key, val);
        } else {
            return appRegistry.get(0).getVal();
        }
        return val;
    }

    public void setRegistry(int key, String val) {
        AppRegistry ar = new AppRegistry();
        ar.setKey(key);
        ar.setVal(val);
        registryBuffer.put(key, val);
        if (onRegistryChange != null) {
            onRegistryChange.onRegistryChange(key, val);
        }
        Intent appIntent = new Intent(this, PlaybackService.class);
        switch (key) {
            case App.SHUFFLE:
                if (val.equals("1")) {
                    appIntent.setAction(PlaybackService.SHUFFLE_ON);
                } else {
                    appIntent.setAction(PlaybackService.SHUFFLE_OFF);
                }
                break;
            case App.CROSSFADE:
                if (val.equals("1")) {
                    appIntent.setAction(PlaybackService.CROSSFADE_ON);
                } else {
                    appIntent.setAction(PlaybackService.CROSSFADE_OFF);
                }
                break;
            case App.REPEAT:
                if (Integer.valueOf(val) == PlaybackService.Repeat.NO_REPEAT) {
                    appIntent.setAction(PlaybackService.NO_REPEAT);
                } else if (Integer.valueOf(val) == PlaybackService.Repeat.REPEAT_ONE) {
                    appIntent.setAction(PlaybackService.REPEAT_ONE);
                } else if (Integer.valueOf(val) == PlaybackService.Repeat.REPEAT_ALL) {
                    appIntent.setAction(PlaybackService.REPEAT_ALL);
                }
                break;
            case App.FREEVERB:
            case App.FREEVERB_DRYMIX:
            case App.FREEVERB_WETMIX:
            case App.FREEVERB_DAMP:
            case App.FREEVERB_ROOMSIZE:
            case App.FREEVERB_WIDTH:
            case App.FREEVERB_MODE:
            case App.PEAEQ:
            case App.CHORUS:
            case App.CHORUS_DRYMIX:
            case App.CHORUS_WETMIX:
            case App.CHORUS_FEEDBACK:
            case App.CHORUS_MINSWEEP:
            case App.CHORUS_MAXSWEEP:
            case App.CHORUS_RATE:
            case App.VOLUME:
            case App.COMPRESSOR:
            case App.COMPRESSOR_GAIN:
            case App.COMPRESSOR_THRESHOLD:
            case App.COMPRESSOR_RATIO:
            case App.COMPRESSOR_ATTACK:
            case App.COMPRESSOR_RELEASE:
            case App.DAMP:
            case App.DAMP_TARGET:
            case App.DAMP_QUIET:
            case App.DAMP_RATE:
            case App.DAMP_GAIN:
            case App.DAMP_DELAY:
                appIntent.setAction(PlaybackService.APPLY_EFFECT);
                break;
            case App.PEAEQ_STRING:
                if (onEqualizerChange != null) {
                    onEqualizerChange.onEqualizerChange();
                }
                appIntent.setAction(PlaybackService.APPLY_EFFECT);
                break;
            case App.GAIN:
                appIntent.setAction(PlaybackService.GAIN);
                break;
        }
        startService(appIntent);
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(ar);
        realm.commitTransaction();
    }

    public void setOnEqualizerChange(OnEqualizerChange onEqualizerChange) {
        this.onEqualizerChange = onEqualizerChange;
    }

    public void setOnRegistryChange(OnRegistryChange onRegistryChange) {
        this.onRegistryChange = onRegistryChange;
    }

    public String listToString(List<EqItem> eqItemList) {
        Gson gson = new Gson();
        return gson.toJson(eqItemList);
    }

    public void saveEq(List<EqItem> eqItemList) {
        Gson gson = new Gson();
        setRegistry(App.PEAEQ_STRING, gson.toJson(eqItemList));
    }

    public String eqFlat() {
        List<EqItem> flatEqList = new ArrayList<>();
        flatEqList.add(new EqItem(0, (float) 0.7, 32));
        flatEqList.add(new EqItem(0, 1, 64));
        flatEqList.add(new EqItem(0, 1, 125));
        flatEqList.add(new EqItem(0, 1, 250));
        flatEqList.add(new EqItem(0, 1, 500));
        flatEqList.add(new EqItem(0, 1, 1000));
        flatEqList.add(new EqItem(0, 1, 2000));
        flatEqList.add(new EqItem(0, 1, 4000));
        flatEqList.add(new EqItem(0, 1, 8000));
        flatEqList.add(new EqItem(0, (float) 0.7, 16000));
        return listToString(flatEqList);
    }

    public String eqAcoustic() {
        List<EqItem> flatEqList = new ArrayList<>();
        flatEqList.add(new EqItem((float) 4.5, (float) 0.7, 32));
        flatEqList.add(new EqItem((float) 4.5, 1, 64));
        flatEqList.add(new EqItem(1, 1, 125));
        flatEqList.add(new EqItem(1, 1, 250));
        flatEqList.add(new EqItem((float) 1.5, 1, 500));
        flatEqList.add(new EqItem((float) 1.5, 1, 1000));
        flatEqList.add(new EqItem(3, 1, 2000));
        flatEqList.add(new EqItem(3, 1, 4000));
        flatEqList.add(new EqItem(2, 1, 8000));
        flatEqList.add(new EqItem(2, (float) 0.7, 16000));
        return listToString(flatEqList);
    }

    public String eqBass() {
        List<EqItem> flatEqList = new ArrayList<>();
        flatEqList.add(new EqItem(5, (float) 0.7, 32));
        flatEqList.add(new EqItem(5, 1, 64));
        flatEqList.add(new EqItem((float) 3.5, 1, 125));
        flatEqList.add(new EqItem((float) 3.5, 1, 250));
        flatEqList.add(new EqItem((float) 1.5, 1, 500));
        flatEqList.add(new EqItem((float) 1.5, 1, 1000));
        flatEqList.add(new EqItem((float) 0.5, 1, 2000));
        flatEqList.add(new EqItem((float) 0.5, 1, 4000));
        flatEqList.add(new EqItem((float) -0.5, 1, 8000));
        flatEqList.add(new EqItem((float) -0.5, (float) 0.7, 16000));
        return listToString(flatEqList);
    }

    public String eqClassical() {
        List<EqItem> flatEqList = new ArrayList<>();
        flatEqList.add(new EqItem(0, (float) 0.7, 32));
        flatEqList.add(new EqItem(0, 1, 64));
        flatEqList.add(new EqItem(0, 1, 125));
        flatEqList.add(new EqItem(0, 1, 250));
        flatEqList.add(new EqItem(0, 1, 500));
        flatEqList.add(new EqItem(0, 1, 1000));
        flatEqList.add(new EqItem(0, 1, 2000));
        flatEqList.add(new EqItem(-7, 1, 4000));
        flatEqList.add(new EqItem(-7, 1, 8000));
        flatEqList.add(new EqItem(-7, (float) 0.7, 16000));
        return listToString(flatEqList);
    }

    public String eqClub() {
        List<EqItem> flatEqList = new ArrayList<>();
        flatEqList.add(new EqItem(0, (float) 0.7, 32));
        flatEqList.add(new EqItem(0, 1, 64));
        flatEqList.add(new EqItem(8, 1, 125));
        flatEqList.add(new EqItem((float) 5.5, 1, 250));
        flatEqList.add(new EqItem((float) 5.5, 1, 500));
        flatEqList.add(new EqItem((float) 5.5, 1, 1000));
        flatEqList.add(new EqItem(3, 1, 2000));
        flatEqList.add(new EqItem(0, 1, 4000));
        flatEqList.add(new EqItem(0, 1, 8000));
        flatEqList.add(new EqItem(0, (float) 0.7, 16000));
        return listToString(flatEqList);
    }

    public String eqDance() {
        List<EqItem> flatEqList = new ArrayList<>();
        flatEqList.add(new EqItem((float) 9.5, (float) 0.7, 32));
        flatEqList.add(new EqItem(7, 1, 64));
        flatEqList.add(new EqItem((float) 2.5, 1, 125));
        flatEqList.add(new EqItem(0, 1, 250));
        flatEqList.add(new EqItem(0, 1, 500));
        flatEqList.add(new EqItem((float) -5.5, 1, 1000));
        flatEqList.add(new EqItem(-7, 1, 2000));
        flatEqList.add(new EqItem(-7, 1, 4000));
        flatEqList.add(new EqItem(0, 1, 8000));
        flatEqList.add(new EqItem(0, (float) 0.7, 16000));
        return listToString(flatEqList);
    }

    public String eqDisco() {
        List<EqItem> flatEqList = new ArrayList<>();
        flatEqList.add(new EqItem(3, (float) 0.7, 32));
        flatEqList.add(new EqItem(3, 1, 64));
        flatEqList.add(new EqItem(1, 1, 125));
        flatEqList.add(new EqItem(1, 1, 250));
        flatEqList.add(new EqItem((float) 4.5, 1, 500));
        flatEqList.add(new EqItem((float) 4.5, 1, 1000));
        flatEqList.add(new EqItem((float) 2.5, 1, 2000));
        flatEqList.add(new EqItem((float) 2.5, 1, 4000));
        flatEqList.add(new EqItem(1, 1, 8000));
        flatEqList.add(new EqItem(1, (float) 0.7, 16000));
        return listToString(flatEqList);
    }

    public String eqElectronic() {
        List<EqItem> flatEqList = new ArrayList<>();
        flatEqList.add(new EqItem((float) 4.5, (float) 0.7, 32));
        flatEqList.add(new EqItem((float) 4.5, 1, 64));
        flatEqList.add(new EqItem((float) 0.5, 1, 125));
        flatEqList.add(new EqItem((float) 0.5, 1, 250));
        flatEqList.add(new EqItem((float) 2.5, 1, 500));
        flatEqList.add(new EqItem((float) 2.5, 1, 1000));
        flatEqList.add(new EqItem((float) 1.5, 1, 2000));
        flatEqList.add(new EqItem((float) 1.5, 1, 4000));
        flatEqList.add(new EqItem(0, 1, 8000));
        flatEqList.add(new EqItem((float) 5.5, (float) 0.7, 16000));
        return listToString(flatEqList);
    }

    public String eqFullBassTreble() {
        List<EqItem> flatEqList = new ArrayList<>();
        flatEqList.add(new EqItem(7, (float) 0.7, 32));
        flatEqList.add(new EqItem((float) 5.5, 1, 64));
        flatEqList.add(new EqItem(0, 1, 125));
        flatEqList.add(new EqItem(-7, 1, 250));
        flatEqList.add(new EqItem(-5, 1, 500));
        flatEqList.add(new EqItem((float) 1.5, 1, 1000));
        flatEqList.add(new EqItem(8, 1, 2000));
        flatEqList.add(new EqItem(11, 1, 4000));
        flatEqList.add(new EqItem(12, 1, 8000));
        flatEqList.add(new EqItem(12, (float) 0.7, 16000));
        return listToString(flatEqList);
    }

    public String eqFullBass() {
        List<EqItem> flatEqList = new ArrayList<>();
        flatEqList.add(new EqItem(-8, (float) 0.7, 32));
        flatEqList.add(new EqItem((float) 9.5, 1, 64));
        flatEqList.add(new EqItem((float) 9.5, 1, 125));
        flatEqList.add(new EqItem((float) 5.5, 1, 250));
        flatEqList.add(new EqItem((float) 1.5, 1, 500));
        flatEqList.add(new EqItem(-4, 1, 1000));
        flatEqList.add(new EqItem(-8, 1, 2000));
        flatEqList.add(new EqItem(-10, 1, 4000));
        flatEqList.add(new EqItem(-11, 1, 8000));
        flatEqList.add(new EqItem(-11, (float) 0.7, 16000));
        return listToString(flatEqList);
    }

    public String eqFullTreble() {
        List<EqItem> flatEqList = new ArrayList<>();
        flatEqList.add(new EqItem((float) -9.5, (float) 0.7, 32));
        flatEqList.add(new EqItem((float) -9.5, 1, 64));
        flatEqList.add(new EqItem((float) -9.5, 1, 125));
        flatEqList.add(new EqItem(-4, 1, 250));
        flatEqList.add(new EqItem((float) 2.5, 1, 500));
        flatEqList.add(new EqItem(11, 1, 1000));
        flatEqList.add(new EqItem(14, 1, 2000));
        flatEqList.add(new EqItem(14, 1, 4000));
        flatEqList.add(new EqItem(14, 1, 8000));
        flatEqList.add(new EqItem(15, (float) 0.7, 16000));
        return listToString(flatEqList);
    }

    public String eqHeadphones() {
        List<EqItem> flatEqList = new ArrayList<>();
        flatEqList.add(new EqItem(5, (float) 0.7, 32));
        flatEqList.add(new EqItem(11, 1, 64));
        flatEqList.add(new EqItem((float) 5.5, 1, 125));
        flatEqList.add(new EqItem(-3, 1, 250));
        flatEqList.add(new EqItem((float) -2.5, 1, 500));
        flatEqList.add(new EqItem((float) 1.5, 1, 1000));
        flatEqList.add(new EqItem(5, 1, 2000));
        flatEqList.add(new EqItem((float) 9.5, 1, 4000));
        flatEqList.add(new EqItem(13, 1, 8000));
        flatEqList.add(new EqItem((float) 14.5, (float) 0.7, 16000));
        return listToString(flatEqList);
    }

    public String eqHipHop() {
        List<EqItem> flatEqList = new ArrayList<>();
        flatEqList.add(new EqItem((float) 6.5, (float) 0.7, 32));
        flatEqList.add(new EqItem((float) 6.5, 1, 64));
        flatEqList.add(new EqItem((float) 2.5, 1, 125));
        flatEqList.add(new EqItem((float) 2.5, 1, 250));
        flatEqList.add(new EqItem(-1, 1, 500));
        flatEqList.add(new EqItem(-1, 1, 1000));
        flatEqList.add(new EqItem((float) 1.5, 1, 2000));
        flatEqList.add(new EqItem((float) 1.5, 1, 4000));
        flatEqList.add(new EqItem((float) 3.5, 1, 8000));
        flatEqList.add(new EqItem((float) 3.5, (float) 0.7, 16000));
        return listToString(flatEqList);
    }

    public String eqJazz() {
        List<EqItem> flatEqList = new ArrayList<>();
        flatEqList.add(new EqItem(4, (float) 0.7, 32));
        flatEqList.add(new EqItem(4, 1, 64));
        flatEqList.add(new EqItem((float) 1.5, 1, 125));
        flatEqList.add(new EqItem((float) 1.5, 1, 250));
        flatEqList.add(new EqItem((float) -2.5, 1, 500));
        flatEqList.add(new EqItem((float) -2.5, 1, 1000));
        flatEqList.add(new EqItem((float) 0.5, 1, 2000));
        flatEqList.add(new EqItem((float) 0.5, 1, 4000));
        flatEqList.add(new EqItem(6, 1, 8000));
        flatEqList.add(new EqItem(6, (float) 0.7, 16000));
        return listToString(flatEqList);
    }

    public String eqLargeHall() {
        List<EqItem> flatEqList = new ArrayList<>();
        flatEqList.add(new EqItem((float) 10.5, (float) 0.7, 32));
        flatEqList.add(new EqItem((float) 10.5, 1, 64));
        flatEqList.add(new EqItem((float) 5.5, 1, 125));
        flatEqList.add(new EqItem((float) 5.5, 1, 250));
        flatEqList.add(new EqItem(0, 1, 500));
        flatEqList.add(new EqItem(-5, 1, 1000));
        flatEqList.add(new EqItem(-5, 1, 2000));
        flatEqList.add(new EqItem(-5, 1, 4000));
        flatEqList.add(new EqItem(0, 1, 8000));
        flatEqList.add(new EqItem(0, (float) 0.7, 16000));
        return listToString(flatEqList);
    }

    public String eqLive() {
        List<EqItem> flatEqList = new ArrayList<>();
        flatEqList.add(new EqItem(-5, (float) 0.7, 32));
        flatEqList.add(new EqItem(0, 1, 64));
        flatEqList.add(new EqItem(4, 1, 125));
        flatEqList.add(new EqItem((float) 5.5, 1, 250));
        flatEqList.add(new EqItem((float) 5.5, 1, 500));
        flatEqList.add(new EqItem((float) 5.5, 1, 1000));
        flatEqList.add(new EqItem(4, 1, 2000));
        flatEqList.add(new EqItem((float) 2.5, 1, 4000));
        flatEqList.add(new EqItem((float) 2.5, 1, 8000));
        flatEqList.add(new EqItem((float) 2.5, (float) 0.7, 16000));
        return listToString(flatEqList);
    }

    public String eqLounge() {
        List<EqItem> flatEqList = new ArrayList<>();
        flatEqList.add(new EqItem((float) -2.5, (float) 0.7, 32));
        flatEqList.add(new EqItem((float) -2.5, 1, 64));
        flatEqList.add(new EqItem((float) 0.5, 1, 125));
        flatEqList.add(new EqItem((float) 0.5, 1, 250));
        flatEqList.add(new EqItem(2, 1, 500));
        flatEqList.add(new EqItem(2, 1, 1000));
        flatEqList.add(new EqItem((float) -1.5, 1, 2000));
        flatEqList.add(new EqItem((float) -1.5, 1, 4000));
        flatEqList.add(new EqItem((float) 1.5, 1, 8000));
        flatEqList.add(new EqItem((float) 1.5, (float) 0.7, 16000));
        return listToString(flatEqList);
    }

    public String eqParty() {
        List<EqItem> flatEqList = new ArrayList<>();
        flatEqList.add(new EqItem(7, (float) 0.7, 32));
        flatEqList.add(new EqItem(7, 1, 64));
        flatEqList.add(new EqItem(0, 1, 125));
        flatEqList.add(new EqItem(0, 1, 250));
        flatEqList.add(new EqItem(0, 1, 500));
        flatEqList.add(new EqItem(0, 1, 1000));
        flatEqList.add(new EqItem(0, 1, 2000));
        flatEqList.add(new EqItem(0, 1, 4000));
        flatEqList.add(new EqItem(7, 1, 8000));
        flatEqList.add(new EqItem(7, (float) 0.7, 16000));
        return listToString(flatEqList);
    }

    public String eqPop() {
        List<EqItem> flatEqList = new ArrayList<>();
        flatEqList.add(new EqItem((float) -1.5, (float) 0.7, 32));
        flatEqList.add(new EqItem(5, 1, 64));
        flatEqList.add(new EqItem(7, 1, 125));
        flatEqList.add(new EqItem(8, 1, 250));
        flatEqList.add(new EqItem((float) 5.5, 1, 500));
        flatEqList.add(new EqItem(0, 1, 1000));
        flatEqList.add(new EqItem((float) -2.5, 1, 2000));
        flatEqList.add(new EqItem((float) -2.5, 1, 4000));
        flatEqList.add(new EqItem((float) 1.5, 1, 8000));
        flatEqList.add(new EqItem((float) 1.5, (float) 0.7, 16000));
        return listToString(flatEqList);
    }

    public String eqRNB() {
        List<EqItem> flatEqList = new ArrayList<>();
        flatEqList.add(new EqItem((float) 3.5, (float) 0.7, 32));
        flatEqList.add(new EqItem((float) 3.5, 1, 64));
        flatEqList.add(new EqItem((float) 4.5, 1, 125));
        flatEqList.add(new EqItem((float) 4.5, 1, 250));
        flatEqList.add(new EqItem((float) 0.5, 1, 500));
        flatEqList.add(new EqItem((float) 0.5, 1, 1000));
        flatEqList.add(new EqItem((float) 2.5, 1, 2000));
        flatEqList.add(new EqItem((float) 2.5, 1, 4000));
        flatEqList.add(new EqItem(3, 1, 8000));
        flatEqList.add(new EqItem(3, (float) 0.7, 16000));
        return listToString(flatEqList);
    }

    public String eqReggae() {
        List<EqItem> flatEqList = new ArrayList<>();
        flatEqList.add(new EqItem(0, (float) 0.7, 32));
        flatEqList.add(new EqItem(0, 1, 64));
        flatEqList.add(new EqItem(0, 1, 125));
        flatEqList.add(new EqItem((float) -5.5, 1, 250));
        flatEqList.add(new EqItem(0, 1, 500));
        flatEqList.add(new EqItem((float) 6.5, 1, 1000));
        flatEqList.add(new EqItem((float) 6.5, 1, 2000));
        flatEqList.add(new EqItem(0, 1, 4000));
        flatEqList.add(new EqItem(0, 1, 8000));
        flatEqList.add(new EqItem(0, (float) 0.7, 16000));
        return listToString(flatEqList);
    }

    public String eqRock() {
        List<EqItem> flatEqList = new ArrayList<>();
        flatEqList.add(new EqItem(5, (float) 0.7, 32));
        flatEqList.add(new EqItem(4, 1, 64));
        flatEqList.add(new EqItem(2, 1, 125));
        flatEqList.add(new EqItem((float) 0.5, 1, 250));
        flatEqList.add(new EqItem((float) -0.5, 1, 500));
        flatEqList.add(new EqItem((float) -0.5, 1, 1000));
        flatEqList.add(new EqItem((float) 0.5, 1, 2000));
        flatEqList.add(new EqItem(2, 1, 4000));
        flatEqList.add(new EqItem(3, 1, 8000));
        flatEqList.add(new EqItem(4, (float) 0.7, 16000));
        return listToString(flatEqList);
    }

    public String eqSka() {
        List<EqItem> flatEqList = new ArrayList<>();
        flatEqList.add(new EqItem((float) -2.5, (float) 0.7, 32));
        flatEqList.add(new EqItem(-5, 1, 64));
        flatEqList.add(new EqItem(-4, 1, 125));
        flatEqList.add(new EqItem(0, 1, 250));
        flatEqList.add(new EqItem(4, 1, 500));
        flatEqList.add(new EqItem((float) 5.5, 1, 1000));
        flatEqList.add(new EqItem(9, 1, 2000));
        flatEqList.add(new EqItem((float) 9.5, 1, 4000));
        flatEqList.add(new EqItem(11, 1, 8000));
        flatEqList.add(new EqItem((float) 9.5, (float) 0.7, 16000));
        return listToString(flatEqList);
    }

    public String eqSoftRock() {
        List<EqItem> flatEqList = new ArrayList<>();
        flatEqList.add(new EqItem(4, (float) 0.7, 32));
        flatEqList.add(new EqItem(4, 1, 64));
        flatEqList.add(new EqItem((float) 2.5, 1, 125));
        flatEqList.add(new EqItem(0, 1, 250));
        flatEqList.add(new EqItem(-4, 1, 500));
        flatEqList.add(new EqItem((float) -5.5, 1, 1000));
        flatEqList.add(new EqItem(3, 1, 2000));
        flatEqList.add(new EqItem(0, 1, 4000));
        flatEqList.add(new EqItem((float) 2.5, 1, 8000));
        flatEqList.add(new EqItem(9, (float) 0.7, 16000));
        return listToString(flatEqList);
    }

    public String eqSoft() {
        List<EqItem> flatEqList = new ArrayList<>();
        flatEqList.add(new EqItem(5, (float) 0.7, 32));
        flatEqList.add(new EqItem((float) 1.5, 1, 64));
        flatEqList.add(new EqItem(0, 1, 125));
        flatEqList.add(new EqItem((float) -2.5, 1, 250));
        flatEqList.add(new EqItem(0, 1, 500));
        flatEqList.add(new EqItem(4, 1, 1000));
        flatEqList.add(new EqItem(8, 1, 2000));
        flatEqList.add(new EqItem((float) 9.5, 1, 4000));
        flatEqList.add(new EqItem(11, 1, 8000));
        flatEqList.add(new EqItem(12, (float) 0.7, 16000));
        return listToString(flatEqList);
    }

    public String eqTechno() {
        List<EqItem> flatEqList = new ArrayList<>();
        flatEqList.add(new EqItem(8, (float) 0.7, 32));
        flatEqList.add(new EqItem((float) 5.5, 1, 64));
        flatEqList.add(new EqItem(0, 1, 125));
        flatEqList.add(new EqItem((float) -5.5, 1, 250));
        flatEqList.add(new EqItem(5, 1, 500));
        flatEqList.add(new EqItem(0, 1, 1000));
        flatEqList.add(new EqItem(8, 1, 2000));
        flatEqList.add(new EqItem((float) 9.5, 1, 4000));
        flatEqList.add(new EqItem((float) 9.5, 1, 8000));
        flatEqList.add(new EqItem(9, (float) 0.7, 16000));
        return listToString(flatEqList);
    }

    public String eqVocal() {
        List<EqItem> flatEqList = new ArrayList<>();
        flatEqList.add(new EqItem((float) -4.5, (float) 0.7, 32));
        flatEqList.add(new EqItem((float) -4.5, 1, 64));
        flatEqList.add(new EqItem((float) 0.5, 1, 125));
        flatEqList.add(new EqItem((float) 0.5, 1, 250));
        flatEqList.add(new EqItem((float) 4.5, 1, 500));
        flatEqList.add(new EqItem((float) 4.5, 1, 1000));
        flatEqList.add(new EqItem((float) 2, 1, 2000));
        flatEqList.add(new EqItem((float) 2, 1, 4000));
        flatEqList.add(new EqItem(0, 1, 8000));
        flatEqList.add(new EqItem(0, (float) 0.7, 16000));
        return listToString(flatEqList);
    }

    public float getRegistryFloat(int idx) {
        return Float.valueOf(getRegistry(idx));
    }

    public int getRegistryInt(int idx) {
        return Integer.valueOf(getRegistry(idx));
    }

    public Realm getRealm() {
        return realm;
    }

    public interface OnEqualizerChange {
        void onEqualizerChange();
    }

    public interface OnRegistryChange {
        void onRegistryChange(int key, String val);
    }
}
